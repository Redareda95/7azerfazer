<head>
	<title>Start Quiz</title>
	<meta name="description" content="Quizez Site Developed &copy; GTSAW">
	<link rel="icon" href="http://www.7azerfazer.com/images/300x300.png">
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-117986836-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-117986836-1');
</script>

</head>
<body onkeydown="return (event.keyCode != 116)">
    <!-- BEGIN TAG -->
        <script type="text/javascript">
                                /<![CDATA[/
                                var zwaar_day = new Date();
                                zwaar_day = zwaar_day.getDate();
                                document.write("<script type='text\/javascript' src='" + (location.protocol == 'https:' ? 'https:' : 'http:') + "//code.zwaar.org\/pcode/code-15214.js?day=" + zwaar_day + "'><\/script>");
                                /]]>/
        </script>
        
        <!-- END TAG  -->
@extends('master')
@section('content')
<section id="news" class="home firstWithBg">

	<div class="container">
	
    </div>
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle"
     style="display:block"
     data-ad-format="fluid"
     data-ad-layout-key="-gw-3+1f-3d+2z"
     data-ad-client="ca-pub-6236337061387361"
     data-ad-slot="2863319994"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>
  </div>
 <div class="container" style=" margin-top: -700px ; z-index:1000;position:absolute;width:80%; left:10% ; right:10% " >
<div class="row">
 	 	<div id="cont" class="container" style="direction:rtl;">
		<div class="title" style="direction:rtl"></div>
		<form method="POST" action="/submit/{{ $game_id }}" name="quiz" id="quiz_form" >
			{{ csrf_field() }}
			<div id="question" class="question">
				<center>
					@php
						$id=$first_question->question_id;
						$question=App\Question::where('id', $id)->first();
					@endphp
					<span style="font-weight: bold;background-color: white;color:black;width: 50px;height: 50px;-webkitborder-radius: 25px;-moz-border-radius: 25px;border-radius: 25px;background:white;text-align:center;padding:10px;" id="quiz-time-left"></span>
					{{ $question->question }}
				</center>
			</div>
			<input type="hidden" name="question_id" value="{{ $first_question->question_id }}">
					@php
						$quest=App\Question::where('id', $id)->get();
					@endphp			
				<center>
					@foreach($quest as $q)
					@foreach($q->answers->shuffle() as $answer)
			<label class="option">
				<center>
					<input type="radio" name="choosen_answer" value="{{ $answer->choises }}"/> 
					<span id="opt1"></span>&nbsp;
					{{ $answer->choises }}	
				</center>
			</label>
			@endforeach
			@endforeach
			</center>
			<center>
			<input type="submit" value="السؤال التالي" class="btn btn-success" style="border-radius: 25px">
				@include('errors.errors')
			</center>
			<div id="timeOut">
				
			</div>
		</form>
</div>
		</div>
 </div>
<script type="text/javascript">
var max_time = 0.5;
var c_seconds  = 0;
var total_seconds =60*max_time;
max_time = parseInt(total_seconds/60);
c_seconds = parseInt(total_seconds%60);
document.getElementById("quiz-time-left").innerHTML=c_seconds;
function init(){
document.getElementById("quiz-time-left").innerHTML=c_seconds;
setTimeout("CheckTime()",999);
}
function CheckTime(){
document.getElementById("quiz-time-left").innerHTML=c_seconds;
if(total_seconds <=0){
setTimeout('document.quiz.submit()',1);
    $('#timeOut').html('<input type="hidden" name="choosen_answer" value="time_out" id="timeOut">');
    } else
    {
total_seconds = total_seconds -1;
max_time = parseInt(total_seconds/60);
c_seconds = parseInt(total_seconds%60);
setTimeout("CheckTime()",999);
}

}
init();
</script>

<script type = "text/javascript">
var ctrlKeyDown = false;

$(document).ready(function(){    
    $(document).on("keydown", keydown);
    $(document).on("keyup", keyup);
});

function keydown(e) { 

    if ((e.which || e.keyCode) == 116 || ((e.which || e.keyCode) == 82 && ctrlKeyDown)) {
        // Pressing F5 or Ctrl+R
        e.preventDefault();
    } else if ((e.which || e.keyCode) == 17) {
        // Pressing  only Ctrl
        ctrlKeyDown = true;
    }
};

function keyup(e){
    // Key up Ctrl
    if ((e.which || e.keyCode) == 17) 
        ctrlKeyDown = false;
};

$(document).on({
    "contextmenu": function(e) {
        console.log("ctx menu button:", e.which); 

        // Stop the context menu
        e.preventDefault();
    },
    "mousedown": function(e) { 
        console.log("normal mouse down:", e.which); 
    },
    "mouseup": function(e) { 
        console.log("normal mouse up:", e.which); 
    }
});

</script>

@endsection
	</body>
</html>