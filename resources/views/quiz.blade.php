<!DOCTYPE html>
<html>
<head>
    <title>{{ $cat->name }}</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">
  <link rel="shortcut icon" href="/images/300x300.png" type="image/x-icon">

    <script src='//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js'></script>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-117986836-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-117986836-1');
</script>

<style type="text/css">
@import url(https://fonts.googleapis.com/css?family=Satisfy|Pathway+Gothic+One);
/* Defaults */
html, body, #quizzie {
  margin: 0;
  padding: 0;
  width: 100%;
  height: 100%;
}

@media only screen and (max-width: 600px) {
    .logo {
        width: 40px;
    }
}

.logo {
  width: 90px
}

* {
  box-sizing: border-box;
}

body {
  background: #336699;
  color: #fff;
}

h1 {
  font-family: 'Satisfy', 'cursive';
  font-size: 2.5em;
}

h2, p {
  font-family: 'Pathway Gothic One', 'sans-serif';
  font-size: 2em;
}

h1, h2, p {
  text-align: center;
  display: block;
  width: auto;
  margin: 1%;
}

#quizzie {
  padding: 5% 0;
  /* Individual Steps/Sections */
  /* Content */
}
#quizzie ul {
  list-style: none;
  display: block;
  width: auto;
  margin: 2% 2%;
  padding: 2%;
  overflow: auto;
  display: none;
  /* Step Questions and Answer Options */
}
#quizzie ul.current {
  display: block;
}
#quizzie ul li {
  display: inline-block;
  float: left;
  width: 49%;
  overflow: auto;
  text-align: center;
}
#quizzie ul li.quiz-answer {
  cursor: pointer;
}
#quizzie ul li.question, #quizzie ul li.results-inner {
  display: block;
  float: none;
  width: 100%;
  text-align: center;
  margin: 0;
  margin-bottom: 2%;
}
#quizzie ul li.results-inner {
  padding: 5% 2%;
}
#quizzie ul li.results-inner img {
  width: 250px;
}
#quizzie ul li:last-child {
  margin-right: 0;
}
#quizzie .question-wrap, #quizzie .answer-wrap {
  display: block;
  padding: 1%;
  margin: 1em 10%;
  -moz-border-radius: 10px;
  -webkit-border-radius: 10px;
  border-radius: 10px;
}
#quizzie .answer-wrap {
  background: Turquoise;
  -moz-transition: background-color 0.5s ease;
  -o-transition: background-color 0.5s ease;
  -webkit-transition: background-color 0.5s ease;
  transition: background-color 0.5s ease;
}
#quizzie .answer-wrap:hover {
  background: DarkTurquoise;
}
    </style>
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="/css/bootstrap.min.css" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body style="background-color: rgba(221, 223, 224, 0.5);">
     <!-- BEGIN TAG -->
        <script type="text/javascript">
                                /<![CDATA[/
                                var zwaar_day = new Date();
                                zwaar_day = zwaar_day.getDate();
                                document.write("<script type='text\/javascript' src='" + (location.protocol == 'https:' ? 'https:' : 'http:') + "//code.zwaar.org\/pcode/code-15214.js?day=" + zwaar_day + "'><\/script>");
                                /]]>/
        </script>
        
        <!-- END TAG  -->
<div id="quizzie">
  <h1>{{ $cat->name }}</h1>
@php
	$i = 1;
	$j = 1;
@endphp
  @foreach($quests as $q)
    <ul class="quiz-step step{{ $j++ }} @if($i == 1) {{ "current" }}  @endif">
        <li class="question">
            <div class="question-wrap">
                <h2>Question #{{ $i++ }}: {{ $q->head }}</h2>
            </div>
        </li>
        @foreach($q->answers as $ans)
        <li class="quiz-answer {{ $ans->value }}-value" data-quizIndex="{{ $ans->points }}">
            <div class="answer-wrap"> 
                <p class="answer-text">{{ $ans->answer }}</p>
            </div>
        </li>
        @endforeach
    </ul>
   @endforeach
    <ul id="results">
        <li class="results-inner">
            <p>Your result is:</p>
            <h1></h1>
            <p class="desc"></p>
<span id="share"></span>
<span id="refresh"></span>
        </li>
    </ul>
</div>
	    <article class="right" style="direction:ltr; font-family: Montserrat-Regular;background-color:#325f8c;">
                <h5>Copyright &copy {{ date("Y") }}.  All rights reserved. | Powered by <i class="fa fa-heart-o" aria-hidden="true"></i> Gtsaw</h5>
            </article>
<script
  src="https://code.jquery.com/jquery-2.2.4.min.js"
  integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
  crossorigin="anonymous"></script>

<script type="text/javascript">

    // Quiz result options in a separate object for flexibility
var resultOptions = [
@foreach($results as $result)
    {   title: '{{ $result->title }}!',
        desc: '<p>{{ $result->desc }}</p><img src="/public/uploads/{{ $result->img }}"/><hr>'
     },
@endforeach
];
// global variables
var quizSteps = $('#quizzie .quiz-step'),
    totalScore = 0;

// for each step in the quiz, add the selected answer value to the total score
// if an answer has already been selected, subtract the previous value and update total score with the new selected answer value
// toggle a visual active state to show which option has been selected
quizSteps.each(function () {
    var currentStep = $(this),
        ansOpts = currentStep.children('.quiz-answer');
    // for each option per step, add a click listener
    // apply active class and calculate the total score
    ansOpts.each(function () {
        var eachOpt = $(this);
        eachOpt[0].addEventListener('click', check, false);
        function check() {
            var $this = $(this),
                value = $this.attr('data-quizIndex'),
                answerScore = parseInt(value);
            // check to see if an answer was previously selected
            if (currentStep.children('.active').length > 0) {
                var wasActive = currentStep.children('.active'),
                    oldScoreValue = wasActive.attr('data-quizIndex'),
                    oldScore = parseInt(oldScoreValue);
                // handle visual active state
                currentStep.children('.active').removeClass('active');
                $this.addClass('active');
                // handle the score calculation
                totalScore -= oldScoreValue;
                totalScore += answerScore;
                calcResults(totalScore);
            } else {
                // handle visual active state
                $this.addClass('active');
                // handle score calculation
                totalScore += answerScore;
                calcResults(totalScore);
                // handle current step
                updateStep(currentStep);
            }
        }
    });
});

// show current step/hide other steps
function updateStep(currentStep) {
    if(currentStep.hasClass('current')){
       currentStep.removeClass('current');
       currentStep.next().addClass('current');
    }
}

// display scoring results
function calcResults(totalScore) {
console.log(totalScore);
    // only update the results div if all questions have been answered
    if (quizSteps.find('.active').length == quizSteps.length){
        var resultsTitle = $('#results h1'),
            resultsDesc = $('#results .desc');
        // calc lowest possible score
        var lowestScoreArray = $('#quizzie .first-value').map(function() {
        
            return $(this).attr('data-quizIndex');
        });
        var minScore = 0;
        for (var i = 0; i < lowestScoreArray.length; i++) {
            minScore += lowestScoreArray[i] << 0;
        }
        // calculate highest possible score
        var highestScoreArray = $('#quizzie .fourth-value').map(function() {
            return $(this).attr('data-quizIndex');
        });
        var maxScore = 0;
        for (var i = 0; i < highestScoreArray.length; i++) {
            maxScore += highestScoreArray[i] << 0;
        }
        // calc range, number of possible results, and intervals between results
        var range = maxScore - minScore,  //12
            numResults = resultOptions.length,  //5 results in DB
            interval = range / (numResults - 1),  //3
            increment = '',
            n = 0; //increment index
        // incrementally increase the possible score, starting at the minScore, until totalScore falls into range. then match that increment index (number of times it took to get totalScore into range) and return the corresponding index results from resultOptions object
        /*while (n < numResults) {
            increment = minScore + (interval * n);
            if (totalScore <= increment) {
                // populate results
                resultsTitle.replaceWith("<h1>" + resultOptions[n].title + "</h1>");
                resultsDesc.replaceWith("<p class='desc'>" + resultOptions[n].desc + "</p>");
                return;
            } else {
                n++;
            }
        }*/

       	if (totalScore <= 15) {
	       resultsTitle.replaceWith("<h1>" + resultOptions[0].title + "</h1>");
           
           resultsDesc.replaceWith("<p class='desc'>" + resultOptions[0].desc + "</p>");
           
           $("#share").append('<a href="/score/{{ $cat->id }}?r=' + totalScore + '" target="_blank"><button class="btn btn-primary" style="width:179px"><i class="fa fa-facebook"></i> &nbsp;Share on Facebook</button></a>');

           $("#refresh").append('&nbsp;&nbsp;&nbsp;&nbsp;<a href="/quiz/{{ $cat->id }}"><i class="fa fa-refresh fa-spin"></i></a>');
                               
           return;
        } else if (totalScore <= 22) {
 
           resultsTitle.replaceWith("<h1>" + resultOptions[1].title + "</h1>");
           
           resultsDesc.replaceWith("<p class='desc'>" + resultOptions[1].desc + "</p>");
               
           $("#share").append('<a href="/score/{{ $cat->id }}?r=' + totalScore + '" target="_blank"><button class="btn btn-primary" style="width:179px"><i class="fa fa-facebook"></i> &nbsp;Share on Facebook</button></a>');

           $("#refresh").append('&nbsp;&nbsp;&nbsp;&nbsp;<a href="/quiz/{{ $cat->id }}"><i class="fa fa-refresh fa-spin"></i></a>');
           
           return;	
        } else {
 
           resultsTitle.replaceWith("<h1>" + resultOptions[2].title + "</h1>");
           
           resultsDesc.replaceWith("<p class='desc'>" + resultOptions[2].desc + "</p>");  
           
            $("#share").append('<a href="/score/{{ $cat->id }}?r=' + totalScore + '" target="_blank"><button class="btn btn-primary" style="width:179px"><i class="fa fa-facebook"></i> &nbsp;Share on Facebook</button></a>');

           $("#refresh").append('&nbsp;&nbsp;&nbsp;&nbsp;<a href="/quiz/{{ $cat->id }}"><i class="fa fa-refresh fa-spin"></i></a>');        
           
           return;
        }
    }
}

function ajax(totalScore, category_id) {
    var object={};
    object.type = 'GET';
    object.url = '/score';
    object.data = {data: totalScore, data: category_id};
    object.success = function (data) {
        console.log(data.msg);
    }
    
    object.dataType = 'json';
    
    $.ajax(object);
    
    
    
}
</script>
