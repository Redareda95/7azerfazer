@include('layouts.topbar')
@if($flash = session('message'))
	<div class="alert alert-warning" role="alert">
		<b>{{ $flash }}</b>
	</div>	
@endif
@yield('content')

@include('layouts.footer')
