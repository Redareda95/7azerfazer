<!DOCTYPE html>

<html class="no-js"  lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">
<link rel="shortcut icon" href="/images/300x300.png" type="image/x-icon">
<title>Quizzat - 7azer Fazer</title>

<!-- Normalize -->

<link rel="stylesheet" href="/cssRed/assets/normalize.css" type="text/css">

<!-- Bootstrap -->

<link href="/cssRed/assets/bootstrap.min.css" rel="stylesheet" type="text/css">

<!-- Font-awesome.min -->
<link rel="stylesheet" id="fontawesome-css"  href="/wp-content/themes/pixiehuge/assets/css/font-awesome.min5152.css?ver=1.0" type="text/css" media="all" />

<!-- Effet -->

<link rel="stylesheet" href="/cssRed/gallery/foundation.min.css"  type="text/css">
<link rel="stylesheet" type="text/css" href="/cssRed/gallery/set1.css" />

<!-- Main Style -->

<link rel="stylesheet" href="/cssRed/main.css" type="text/css">

<!-- Responsive Style -->

<link href="/cssRed/responsive.css" rel="stylesheet" type="text/css">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->

<!--[if lt IE 9]>

      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>

      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <![endif]-->

<script src="/jss/assets/modernizr-2.8.3.min.js" type="text/javascript"></script>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-117986836-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-117986836-1');
</script>

</head>

<body onload="typeWriter()">
<!-- BEGIN TAG -->
        <script type="text/javascript">
                                /<![CDATA[/
                                var zwaar_day = new Date();
                                zwaar_day = zwaar_day.getDate();
                                document.write("<script type='text\/javascript' src='" + (location.protocol == 'https:' ? 'https:' : 'http:') + "//code.zwaar.org\/pcode/code-15214.js?day=" + zwaar_day + "'><\/script>");
                                /]]>/
        </script>
        
        <!-- END TAG  -->
<!-- header -->

<header id="header" class="header">
  <div class="container-fluid">
    <hgroup style="background-color:#222222;"> 
      <h1> <a href="/" title="Picxa"><img src="/images/300x300.png" alt="Picxa" title="GTSAW" style="border-radius: 25%" /></a> </h1>
      <center><p id="demo" style="color:white;margin-top: 35px; font-size: 28px;"></p></center>
    </hgroup>
  </div>
</header>
<script>
var i = 0;
var txt = 'Welcome To Quizzat Site.';
var speed = 50;

function typeWriter() {
  if (i < txt.length) {
    document.getElementById("demo").innerHTML += txt.charAt(i);
    i++;
    setTimeout(typeWriter, speed);
  }
}
</script>
<main class="main-wrapper" id="container"> 
  <div class="wrapper">
    <div class="">
      <ul class="small-block-grid-2 medium-block-grid-3 large-block-grid-4 masonry">
      @foreach($categories as $category)
        <li class="masonry-item grid">
          <figure class="effect-sarah"> <img src="/public/uploads/{{ $category->image }}" alt="" />
            <figcaption>
              <h2>{{ $category->name }} <span>Quiz</span></h2>
              <p>{{ $category->describtion }}</p>
              <a href="/quiz/{{ $category->id }}">Take This Quiz</a> </figcaption>
          </figure>
        </li>
       @endforeach
      </ul>
    </div>
  </div>
</main>

<!-- footer -->
<footer class="footer">
  <h3>Stay connected with us</h3>
  <div class="container footer-bot">
    <div class="row"> 
      <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3"> <img src="/images/32.png" alt="Picxa" title="GTSAW"/>
        <p class="copy-right">Copyright &copy {{ date("Y") }}.  All rights reserved. | Powered by <i class="fa fa-heart" aria-hidden="true"></i> Gtsaw</p>
      </div>
      <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 padding-top">
        <address>
        <p>13 Saqr Quraish Buildings, Sheraton Heliopolis</p>
        <p>Heliopolis – Cairo</p>
        </address>
      </div>
      <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 padding-top">
        <p><a href="mailto:info@GTSAW.com">contact@GTSAW.com</a></p>
        <p>+2-0100-053-405-9</p>
      </div>
      <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 padding-top">
        <ul class="social">
          <li><a href="https://twitter.com/GTSAW1"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
          <li><a href="https://mail.google.com/mail/u/4/#inbox"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
          <li><a href="https://www.facebook.com/GTSAW.NET/"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
          <li><a href="https://www.linkedin.com/in/gtsaw-software-solution-b59007162/"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
        </ul>
        <p class="made-by">Made with by <i class="fa fa-heart" aria-hidden="true"></i> <a href="http://www.gtsaw.com/" target="_blank">GTSAW</a>
        <p> 
      </div>
    </div>
  </div>
</footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script> 
<script>window.jQuery || document.write('<script src="js/assets/jquery.min.js"><\/script>')</script> 
<script src="/jss/assets/plugins.js" type="text/javascript"></script> 
<script src="/jss/assets/bootstrap.min.js" type="text/javascript"></script>  
<script src="/jss/custom.js" type="text/javascript"></script> 
<script src="/jss/jquery.contact.js" type="text/javascript"></script> 
<script src="/jss/main.js" type="text/javascript"></script> 
<script src="/jss/gallery/masonry.pkgd.min.js" type="text/javascript"></script> 
<script src="/jss/gallery/imagesloaded.pkgd.min.js" type="text/javascript"></script> 
<script src="/jss/gallery/jquery.infinitescroll.min.js" type="text/javascript"></script> 
<script src="/jss/gallery/main.js" type="text/javascript"></script> 
<script src="/jss/jquery.nicescroll.min.js" type="text/javascript"></script>
</body>
</html>