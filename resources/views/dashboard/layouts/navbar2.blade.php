<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="This Admin Panel Is Designed & Developed by Ahmed Reda Abdul-Wahab">
    <meta name="author" content="Ahmed Reda Abdul-Wahab">
  <link rel="shortcut icon" href="/images/300x300.png" type="image/x-icon">

    <title>Admin Panel</title>

    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/dashboard.css" rel="stylesheet">
  
<script
  src="https://code.jquery.com/jquery-3.3.1.js"
  integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
  crossorigin="anonymous"></script>

  </head>

  <body style="margin: -50px auto;">
    <nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="/admin">
        <span data-feather="target"></span>&nbsp;7azar Fazar</a>
      <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
          <a class="nav-link" href="/logout"><button class="btn btn-info"><span data-feather="log-out" style="margin: -5px auto"></span>&nbsp;Sign out</button></a>
        </li>
      </ul>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
          <div class="sidebar-sticky">
            <ul class="nav flex-column">
              <li class="nav-item">
                <a class="nav-link active" href="/admin">
                  <span data-feather="home"></span>
                  Dashboard
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/users">
                  <span data-feather="users"></span>
                  Users
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/addUser">
                  <span data-feather="user-plus"></span>
                  Add User
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/showQuestions">
                  <span data-feather="pocket"></span>
                  Questions
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/addQuestion">
                  <span data-feather="plus-circle"></span>
                  Add Questions 
                </a>
              </li>
               <li class="nav-item">
                <a class="nav-link" href="/7azerResults">
                  <span data-feather="check-circle"></span>
                  Results 
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/statistics">
                  <span data-feather="activity"></span>
                  Questions Statistics
                </a>
              </li>
                <h3>
                  <span data-feather="arrow-down"></span>
                  Quizat Section
                </h3>
                  <ul class="nav flex-column">
                    <li class="nav-item">
                      <a class="nav-link" href="/category">
                        <span data-feather="briefcase"></span>
                        Quizez
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="/addques">
                        <span data-feather="plus"></span>
                        Add Questions to Quiz
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="/results">
                        <span data-feather="plus-square"></span>
                        Add Results
                      </a>
                    </li>
                  </ul>
                  <h3>
                  <span data-feather="arrow-down"></span>
                  Month & Day Game
                </h3>
                  <ul class="nav flex-column">
                    <li class="nav-item">
                      <a class="nav-link" href="/month">
                        <span data-feather="calendar"></span>
                        Month
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="/day">
                        <span data-feather="calendar"></span>
                        Day
                      </a>
                    </li>
                  </ul>
               <li class="nav-item">
                <a class="nav-link" href="/">
                  <button class="btn btn-warning" style="width: 200px">
                  View Site
                  </button>
                </a>
              </li>
            </ul>
          </div>
        </nav>
      </div>
    </div>
