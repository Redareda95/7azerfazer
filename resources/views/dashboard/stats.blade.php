<head>
<title>Stats</title>

</head>
@extends('dashboard.masterAdmin')
@section('admin')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.css">

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
	<h2>All Stats</h2>
	<hr>
@if($flash = session('message'))
	<div class="alert alert-warning" role="alert">
		<b>{{ $flash }}</b>
	</div>	
@endif
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<table class="table table-hover" id="table_id">
			<tr>
				<th>Id</th>
				<th>Question</th>
				<th>Times Asked?</th>
				<th>Answered Correctly</th>
				<th>Answered Wrong</th>
			</tr>
			@foreach($query as $q)
			<tr>
				<td>{{ $q->question_id }}</td>
				<td>{{ $q->questions->question }}</td>
				<td style="text-align:center">{{ $q["COUNT('*')"] }} Time</td>
				@php
				 $correct = App\Score::where('is_correct', 1)
               	   		->where('question_id', $q->question_id)
                    		->count();
                    		
                    		$wrong= App\Score::where('is_correct', 0)
               	   		->where('question_id', $q->question_id)
                    		->count();
                    		
				@endphp
				<td style="text-align:center">{{ $correct }} Time</td>
				<td style="text-align:center">{{ $wrong }} Time</td>
			</tr>
			@endforeach
		</table>
	
	</div>
</main>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
<script>
$(document).ready( function () {
    $('#table_id').DataTable();
} );
$('#table_id').DataTable( {
    paging: true
} );
</script>
@endsection