@extends('dashboard.masterAdmin')
@section('admin')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
										<h2>Edit Month</h2>
										<hr>
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
	<div class="container">
  <form method="POST" action="/editMonth/{{ $month->id }}"  enctype="multipart/form-data">
  	{{ csrf_field() }}
    <input type="hidden" name="_method" value="PATCH">
    <div class="form-group row">
      <label for="Question" class="col-sm-2 col-form-label">Month</label>
      <div class="col-sm-10">
        <input type="text" name='month' class="form-control" id="name" 
        value="{{ $month->month }}">
      </div>
    </div>
    <div class="form-group">
      <h3>Old Image</h3>
      <img src="/public/uploads/{{ $month->image }}" width="215">
    </div>
     <div class="form-group">
      <label for="upload"><b>Upload Image</b></label>
      <input type="file" name="image" class="form-control">
      </div>
      <div class="form-group">
      <label for="body"><b>Result</b></label>
      <input type="text" name='result' class="form-control" id="name" 
        value="{{ $month->result }}">
      </div>
    <div class="form-group row">
      <div class="offset-sm-2 col-sm-10">
        <button type="submit" class="btn btn-success">Edit MOnth</button>
      </div>
    </div>
    @include("errors.errors")
  </form>
</div>
	</div>
</main>
@endsection