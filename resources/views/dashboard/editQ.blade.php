@extends('dashboard.masterAdmin')
@section('admin')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
										<h2>Edit Question</h2>
										<hr>
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
	<div class="container">
  <form method="POST" action="/editQ/{{ $question->id }}">
  	{{ csrf_field() }}
    <input type="hidden" name="_method" value="PATCH">
    <div class="form-group row">
     <label for="sel1" class="col-sm-2 col-form-label">Choose Quiz Name:</label>
      <div class="col-sm-4">
      <select class="form-control" id="sel1" name="category_id">
        @foreach($categories as $category)
        <option value="{{ $category->id }}" <?php ($category->id == $question->category->id)?  'selected' : ' ' ?>>
          {{ $category->name }}
        </option>
        @endforeach
      </select>
    </div>
    </div>
    <div class="form-group row">
      <label for="Question" class="col-sm-2 col-form-label">Question Head</label>
      <div class="col-sm-10">
        <input type="text" name='head' class="form-control" id="name" value="{{ $question->head }}">
      </div>
    </div>
    <div class="form-group row">
      <label for="fakeChoises" class="col-sm-2 col-form-label">Choises</label>
      <div class="col-sm-4">
        @for($i = 0; $i<=3; $i++)
        <input type="text" name="answer{{ $i }}" class="form-control" id="inputAns4" value="{{ $question->answers[$i]->answer }}">
        @endfor
      </div>
      <div class="col-sm-4">
        @for($i = 0; $i<=3; $i++)
        <input type="text" name="point{{ $i }}" class="form-control" id="inputAns4" value="{{ $question->answers[$i]->points }}">
        @endfor
      </div>
    </div>
    <div class="form-group row">
      <div class="offset-sm-2 col-sm-10">
        <button type="submit" class="btn btn-success">Edit Question</button>
      </div>
    </div>
    @include("errors.errors")
  </form>
</div>
</div>
</main>
@endsection