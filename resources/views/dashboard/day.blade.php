@extends('dashboard.masterAdmin')
@section('admin')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
										<h2>Edit Day</h2>
										<hr>
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
	<div class="container">
  <form method="POST" action="/editDay/{{ $day->id }}"  enctype="multipart/form-data">
  	{{ csrf_field() }}
    <input type="hidden" name="_method" value="PATCH">
    <div class="form-group row">
      <label for="Question" class="col-sm-2 col-form-label">Month</label>
      <div class="col-sm-10">
        <input type="text" name='day' class="form-control" id="name" 
        value="{{ $day->day }}">
      </div>
    </div>
      <div class="form-group">
      <label for="body"><b>Result</b></label>
      <input type="text" name='result' class="form-control" id="name" 
        value="{{ $day->result }}">
      </div>
    <div class="form-group row">
      <div class="offset-sm-2 col-sm-10">
        <button type="submit" class="btn btn-success">Edit Day</button>
      </div>
    </div>
    @include("errors.errors")
  </form>
</div>
	</div>
</main>
@endsection