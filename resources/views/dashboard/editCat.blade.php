@extends('dashboard.masterAdmin')
@section('admin')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
										<h2>Edit Category</h2>
										<hr>
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
	<div class="container">
  <form method="POST" action="/editCat/{{ $category->id }}"  enctype="multipart/form-data">
  	{{ csrf_field() }}
    <input type="hidden" name="_method" value="PATCH">
    <div class="form-group row">
      <label for="Question" class="col-sm-2 col-form-label">Category Name</label>
      <div class="col-sm-10">
        <input type="text" name='name' class="form-control" id="name" 
        value="{{ $category->name }}">
      </div>
    </div>
    <div class="form-group">
      <h3>Old Image</h3>
      <img src="/public/uploads/{{ $category->image }}" width="215">
    </div>
     <div class="form-group">
      <label for="upload"><b>Upload Image</b></label>
      <input type="file" name="image" class="form-control">
      </div>
      <div class="form-group">
      <label for="body"><b>Description</b></label>
      <textarea name="describtion" id="describtion" class="form-control" rows="7" cols="10" style="width: 500px">{{ $category->describtion }}</textarea>
      </div>
    <div class="form-group row">
      <div class="offset-sm-2 col-sm-10">
        <button type="submit" class="btn btn-success">Edit Category</button>
      </div>
    </div>
    @include("errors.errors")
  </form>
</div>
	</div>
</main>
@endsection