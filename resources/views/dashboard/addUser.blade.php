@extends('dashboard.masterAdmin')
@section('admin')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
										<h2>Add User</h2>
										<hr>

	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
	<div class="container">
  <form method="POST" action="/registerFromPanel">
  	{{ csrf_field() }}
    <div class="form-group row">
      <label for="name" class="col-sm-2 col-form-label">Name</label>
      <div class="col-sm-10">
        <input type="text" name='name' class="form-control" id="name" placeholder="Name">
      </div>
    </div>
    <div class="form-group row">
      <label for="email" class="col-sm-2 col-form-label">Email</label>
      <div class="col-sm-10">
        <input type="text" name='email' class="form-control" id="inputEmail3" placeholder="Email">
      </div>
    </div>
    <div class="form-group row">
      <label for="inputPassword3" class="col-sm-2 col-form-label">Password</label>
      <div class="col-sm-10">
        <input type="password" name="password" class="form-control" id="inputPassword3" placeholder="Password">
      </div>
    </div>
     <div class="form-group row">
      <label for="inputPassword4" class="col-sm-2 col-form-label">Confirm Password</label>
      <div class="col-sm-10">
        <input type="password" name="password_confirmation" class="form-control" id="inputPassword4" placeholder="Confirm Password">
      </div>
    </div>
    <div class="form-group row">
      <div class="offset-sm-2 col-sm-10">
        <button type="submit" class="btn btn-primary">Add User</button>
      </div>
    </div>
    @include("errors.errors")
  </form>
</div>



	</div>
</main>

@endsection