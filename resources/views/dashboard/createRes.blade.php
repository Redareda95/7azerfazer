@extends('dashboard.masterAdmin')
@section('admin')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
										<h2>Add Result</h2>
										<hr>
@if($flash = session('message'))
  <div class="alert alert-warning" role="alert">
    <b>{{ $flash }}</b>
  </div>  
@endif

	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
	<div class="container">
  <form method="POST" action="/resSto" enctype="multipart/form-data">
  	{{ csrf_field() }}
    <div class="form-group row">
     <label for="sel1" class="col-sm-2 col-form-label">Choose Quiz Name:</label>
      <div class="col-sm-4">
      <select class="form-control" id="sel1" name="category_id">
        <option disabled selected>---</option>
        @foreach($categoriess as $category)
        <option value="{{ $category->id }}">{{ $category->name }}</option>
        @endforeach
      </select>
    </div>
    </div>
    <div class="form-group row">
      <label for="Question" class="col-sm-2 col-form-label">Result Title</label>
      <div class="col-sm-10">
        <input type="text" name='title' class="form-control" id="name" placeholder="Result title">
      </div>
    </div>
    <div class="form-group">
			<label for="upload" class="col-sm-2 col-form-label"><b>Upload Image</b></label>
			<input type="file" name="img" class="form-control">
	</div>
	<div class="form-group">
	<label for="body" class="col-sm-2 col-form-label"><b>Description</b></label>
	<textarea name="desc" id="desc" class="form-control" rows="7" cols="10" style="width: 500px"></textarea>
	</div>
    
    <div class="form-group row">
      <div class="offset-sm-2 col-sm-10">
        <button type="submit" class="btn btn-link">Add Question</button>
      </div>
    </div>
    @include("errors.errors")
  </form>
</div>



	</div>
</main>

@endsection