 @extends('dashboard.masterAdmin')
@section('admin')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
				<h2><u>{{ $game->user->name }}</u> With Email 
          <mark style="background-color: yellow">{{ $game->user->email }}</mark>
        </h2>
										<hr>
@if($flash = session('message'))
	<div class="alert alert-warning" role="alert">
		<b>{{ $flash }}</b>
	</div>	
@endif
<div class="container">
<div class="row">
		<div class="col-sm h3">
      Asked Questions
    	</div>
    	<div class="col-sm h3">
      His Answer
    	</div>
    	<div class="col-sm h3">
      Correct Answer
    	</div>
</div>
</div>
<hr>
<div class="container">
<div class="row">
	  <div class="col-sm">
      @foreach($score as $sco)
      @php
       $questions=App\Question::where('id', $sco->question_id)->value('question');
      @endphp
     	<p>{{ $questions }}</p>
      <hr>
      @endforeach
    </div>

    <div class="col-sm">
      @foreach($score as $sc)
      @php
       $co_ans=App\Question::where('id', $sc->question_id)->value('correct_answer');
      @endphp
      @if($sc->choosen_answer == $co_ans)
        <span data-feather="check"></span>
        @else
        <span data-feather="slash"></span>
      @endif
		    <span style="margin: auto 50px">{{ $sc->choosen_answer }}</span>
        <hr>
	    @endforeach
    </div>

    <div class="col-sm" style="background-color: #FFC7C7">
      @foreach($score as $scor)
      @php
       $c_ans=App\Question::where('id', $scor->question_id)->value('correct_answer');
      @endphp
      <span>{{ $c_ans }}</span>
      <hr>
      @endforeach
    </div>
</div>
</div>

		

</main>
@endsection