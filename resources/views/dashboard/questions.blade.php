@extends('dashboard.masterAdmin')
@section('admin')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
	<h2>All Questions</h2>
	<hr>
@if($flash = session('message'))
	<div class="alert alert-warning" role="alert">
		<b>{{ $flash }}</b>
	</div>	
@endif
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<table class="table table-hover">
			<tr>
				<th>Id</th>
				<th>Category Name</th>
				<th>Question Head</th>
				<th>Answer</th>
				<th>Edit</th>
				<th>Delete</th>
			</tr>
				@foreach($questions as $question)
			<tr>
				<td>{{ $question->id }}</td>
				<td>{{ $question->category->name }}</td>
				<td>{{ $question->head }}</td>
				<td>@foreach($question->answers as $ans)
					{{ $ans->answer }}, 
					@endforeach
				</td>
				<td><a href="/editQ/{{ $question->id }}"><button class="btn btn-success">Edit</button></a></td>
				<td><a href="/deleteQ/{{ $question->id }}"><button class="btn btn-danger">Delete</button></a></td>
			</tr>
				@endforeach			
		</table>
		</div>
			<h2>All Results for Quiz</h2>

	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<table class="table table-hover">
			<tr>
				<th>Id</th>
				<th>Category Name</th>
				<th>Title</th>
				<th>Image</th>
				<th>Desc</th>
				<th>Edit</th>
				<th>Delete</th>
			</tr>
				@foreach($results as $re)
			<tr>
				<td>{{ $re->id }}</td>
				<td>{{ $re->category->name }}</td>
				<td>{{ $re->title }}</td>
				<td><img src="/public/uploads/{{ $re->img}}" width="70"></td>
				<td>{{ $re->desc }}</td>
				<td><a href="/editR/{{ $re->id }}"><button class="btn btn-success">Edit</button></a></td>
				<td><a href="/deleteR/{{ $re->id }}"><button class="btn btn-danger">Delete</button></a></td>
			</tr>
				@endforeach			
		</table>
	</div>
</main>
@endsection