@extends('dashboard.masterAdmin')
@section('admin')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
								<h2>Results</h2>
							<div>
								<a href="/7azerResults?order=asc">
									<button class="btn btn-link">
									Order By Newest
									</button>
								</a>
								||
								<a href="/7azerResults?order=desc">
									<button class="btn btn-link">
									Order By Oldest
									</button>
								</a>
							</div>
										<hr>
@if($flash = session('message'))
	<div class="alert alert-warning" role="alert">
		<b>{{ $flash }}</b>
	</div>	
@endif
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<table class="table table-hover">
	<tr>
		<th>Game ID</th>
		<th>User Email</th>
		<th>Played At</th>
		<th>Result</th>
		<th>See Details</th>
	</tr>
	@foreach($games as $game)
	<tr>
		<td>{{ $game->id }}</td>
		<td>{{ $game->user->email}}</td>
                <td>{{ $game->created_at->diffForHumans() }} </td>
		@php
		 $result = App\Score::where('is_correct', '1')
		 			->where('game_id', $game->id)
		 			->count();
		@endphp
		<td> {{ $result }} Out of 5</td>
		<td>
			<a href="/see_details/{{ $game->id }}"><button class="btn btn-primary">See Details</button></a>
		</td>
	</tr>
	@endforeach
</table>
</div>
</main>
@endsection