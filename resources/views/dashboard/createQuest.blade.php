@extends('dashboard.masterAdmin')
@section('admin')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
										<h2>Add Question</h2>
										<hr>
@if($flash = session('message'))
  <div class="alert alert-warning" role="alert">
    <b>{{ $flash }}</b>
  </div>  
@endif

	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
	<div class="container">
  <form method="POST" action="/addQuest">
  	{{ csrf_field() }}
    <div class="form-group row">
     <label for="sel1" class="col-sm-2 col-form-label">Choose Quiz Name:</label>
      <div class="col-sm-4">
      <select class="form-control" id="sel1" name="category_id">
        <option disabled selected>---</option>
        @foreach($categories as $category)
        <option value="{{ $category->id }}">{{ $category->name }}</option>
        @endforeach
      </select>
    </div>
    </div>
    <div class="form-group row">
      <label for="Question" class="col-sm-2 col-form-label">Question Head</label>
      <div class="col-sm-10">
        <input type="text" name='head' class="form-control" id="name" placeholder="Question Head">
      </div>
    </div>
    <div class="form-group row">
      <label for="fakeChoises" class="col-sm-2 col-form-label">Choises</label>
      <div class="col-sm-4">
        <input type="text" name="answer1" class="form-control" id="inputAns4" placeholder="Choise 1">
        <hr>
        <input type="text" name="answer2" class="form-control" id="inputAns4" placeholder="Choise 2">
        <hr>
        <input type="text" name="answer3" class="form-control" id="inputAns4" placeholder="Choise 3">
        <hr>
        <input type="text" name="answer4" class="form-control" id="inputAns4" placeholder="Choise 4">
        <hr>
      </div>
      <div class="col-sm-4">
        <input type="text" name="point1" class="form-control" id="inputAns4" placeholder="Points">
        <hr>
        <input type="text" name="point2" class="form-control" id="inputAns4" placeholder="Points">
        <hr>
        <input type="text" name="point3" class="form-control" id="inputAns4" placeholder="Points">
        <hr>
        <input type="text" name="point4" class="form-control" id="inputAns4" placeholder="Points">
        <hr>
    
      </div>
    </div>
    <div class="form-group row">
      <div class="offset-sm-2 col-sm-10">
        <button type="submit" class="btn btn-link">Add Question</button>
      </div>
    </div>
    @include("errors.errors")
  </form>
</div>



	</div>
</main>

@endsection