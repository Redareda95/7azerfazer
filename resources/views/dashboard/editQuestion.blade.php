@extends('dashboard.masterAdmin')
@section('admin')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
										<h2>Edit Question</h2>
										<hr>
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
	<div class="container">
  <form method="POST" action="/editQuestion/{{ $quest->id }}">
  	{{ csrf_field() }}
    <input type="hidden" name="_method" value="PATCH">
    <div class="form-group row">
      <label for="Question" class="col-sm-2 col-form-label">Question Head</label>
      <div class="col-sm-10">
        <input type="text" name='question' class="form-control" id="name" 
        value="{{ $quest->question }}">
      </div>
    </div>
    <div class="form-group row">
      <label for="Correct Answer" class="col-sm-2 col-form-label">Correct Answer</label>
      <div class="col-sm-10">
        <input type="text" name='correct_answer' class="form-control" id="inputAns3" 
        value="{{ $quest->correct_answer }}">
      </div>
    </div>
    <div class="form-group row">
      <label for="fakeChoises" class="col-sm-2 col-form-label">Other Fake Choises</label>
      <div class="col-sm-10">
        <input type="text" name="other_choises" class="form-control" id="inputAns4" 
        value="@foreach($quest->answers as $choises){{ $choises->choises }},@endforeach">
        <span><small>*Write each answer followed by Comma(',')</small></span>
      </div>
    </div>
    <div class="form-group row">
      <div class="offset-sm-2 col-sm-10">
        <button type="submit" class="btn btn-success">Edit Question</button>
      </div>
    </div>
    @include("errors.errors")
  </form>
</div>
	</div>
</main>
@endsection