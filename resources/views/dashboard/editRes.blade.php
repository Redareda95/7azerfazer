@extends('dashboard.masterAdmin')
@section('admin')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
										<h2>Edit Result</h2>
										<hr>
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
	<div class="container">
  <form method="POST" action="/editRR/{{ $result->id }}"  enctype="multipart/form-data">
  	{{ csrf_field() }}
    <input type="hidden" name="_method" value="PATCH">
    <div class="form-group row">
     <label for="sel1" class="col-sm-2 col-form-label">Choose Quiz Name:</label>
      <div class="col-sm-4">
      <select class="form-control" id="sel1" name="category_id">
        @foreach($categories as $category)
        <option value="{{ $category->id }}" <?php ($category->id == $result->category_id)?  'selected' : ' ' ?>>
          {{ $category->name }}
        </option>
        @endforeach
      </select>
    </div>
    </div>
    <div class="form-group row">
      <label for="Question" class="col-sm-2 col-form-label">Result Title</label>
      <div class="col-sm-10">
        <input type="text" name='title' class="form-control" id="name" 
        value="{{ $result->title }}">
      </div>
    </div>
    <div class="form-group">
      <h3>Old Image</h3>
      <img src="/public/uploads/{{ $result->img }}" width="215">
    </div>
     <div class="form-group">
      <label for="upload"><b>Upload Image</b></label>
      <input type="file" name="img" class="form-control">
      </div>
      <div class="form-group">
      <label for="body"><b>Description</b></label>
      <textarea name="desc" id="describtion" class="form-control" rows="7" cols="10" style="width: 500px">{{ $result->desc }}</textarea>
      </div>
    <div class="form-group row">
      <div class="offset-sm-2 col-sm-10">
        <button type="submit" class="btn btn-success">Edit Result</button>
      </div>
    </div>
    @include("errors.errors")
  </form>
</div>
	</div>
</main>
@endsection