@extends('dashboard.masterAdmin')
@section('admin')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
	<h2>All Questions</h2>
	<hr>
@if($flash = session('message'))
	<div class="alert alert-warning" role="alert">
		<b>{{ $flash }}</b>
	</div>	
@endif
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<table class="table table-hover">
			<tr>
				<th>Id</th>
				<th>Question</th>
				<th>Correct Answer</th>
				<th>All Answers</th>
				<th>Created At</th>
{{-- 				<th>Edit</th>
 --}}				<th>Delete</th>
			</tr>
				@foreach($questions as $question)
			<tr>
				<td>{{ $question->id }}</td>
				<td>{{ $question->question }}</td>
				<td>{{ $question->correct_answer }}</td>
				@foreach($answers as $answer)
				@php
					$ans=App\Answer::where('question_id', $question->id)->get(['choises']);
				@endphp
				@endforeach
				<td>
				@foreach($ans as $a)
				{{ $a->choises }},
				@endforeach
				</td>
				<td>{{ $question->created_at->toFormattedDateString() }}</td>
				{{-- <td><a href="/edit/{{ $question->id }}"><button class="btn btn-success">Edit</button></a></td> --}}
				<td><a href="/delete/{{ $question->id }}"><button class="btn btn-danger">Delete</button></a></td>
			</tr>
				@endforeach			
				
		</table>
		
	
	</div>
	<div>{{ $questions->render() }}</div>
</main>

@endsection