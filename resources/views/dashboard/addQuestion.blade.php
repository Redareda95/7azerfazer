@extends('dashboard.masterAdmin')
@section('admin')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
										<h2>Add Question</h2>
										<hr>
@if($flash = session('message'))
  <div class="alert alert-warning" role="alert">
    <b>{{ $flash }}</b>
  </div>  
@endif

	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
	<div class="container">
  <form method="POST" action="/addQuestions">
  	{{ csrf_field() }}
    <div class="form-group row">
      <label for="Question" class="col-sm-2 col-form-label">Question Head</label>
      <div class="col-sm-10">
        <input type="text" name='question' class="form-control" id="name" placeholder="Question Head">
      </div>
    </div>
    <div class="form-group row">
      <label for="Correct Answer" class="col-sm-2 col-form-label">Correct Answer</label>
      <div class="col-sm-10">
        <input type="text" name='correct_answer' class="form-control" id="inputAns3" placeholder="Correct Answer">
      </div>
    </div>
    <div class="form-group row">
      <label for="fakeChoises" class="col-sm-2 col-form-label">Other Fake Choises</label>
      <div class="col-sm-10">
        <input type="text" name="answer1" class="form-control" id="inputAns4" placeholder="Fake Choise 1">
        <hr>
        <input type="text" name="answer2" class="form-control" id="inputAns4" placeholder="Fake Choise 2">
        <hr>
        <input type="text" name="answer3" class="form-control" id="inputAns4" placeholder="Fake Choise 3">
        <hr>
        <input type="text" name="answer4" class="form-control" id="inputAns4" placeholder="Fake Choise 4">
      </div>
    </div>
    <div class="form-group row">
      <div class="offset-sm-2 col-sm-10">
        <button type="submit" class="btn btn-info">Add Question</button>
      </div>
    </div>
    @include("errors.errors")
  </form>
</div>



	</div>
</main>

@endsection