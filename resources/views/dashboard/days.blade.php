@extends('dashboard.masterAdmin')
@section('admin')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
	<h2>All Days</h2>
	<hr>
@if($flash = session('message'))
	<div class="alert alert-warning" role="alert">
		<b>{{ $flash }}</b>
	</div>	
@endif
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<table class="table table-hover">
			<tr>
				<th>Month</th>
				<th>Result</th>
				<th>Edit</th>
			</tr>
				@foreach($days as $day)
			<tr>
				<td>{{ $day->day }}</td>
				<td>{{ $day->result }}</td>
				<td><a href="/dayShow/{{ $day->id }}"><button class="btn btn-success">Edit</button></a></td>
			</tr>
				@endforeach			
		</table>
	</div>
	<hr>
</div>
</main>
@endsection