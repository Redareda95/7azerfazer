@extends('dashboard.masterAdmin')
@section('admin')

<body onload="typeWriter()">

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
	<h4 style="padding-top:25px">Dashboard</h4>
	<hr>
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                                        <h1 style="padding-top:60px"><p id="demo"></p></h1>
</div>
</main>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
 <div class="row" style="padding-top:100px">

  <div class="col-sm-3">
    <div style="border: 1px solid #ccc; padding: 40px 25px;width: 100%" class="alert alert-info">
      <span data-feather="users"></span>
      {{ $users }} Users
    </div>
  </div>

  <div class="col-sm-3">
    <div style="border: 1px solid #ccc; padding: 40px 25px;width: 100%;" class="alert alert-warning">
      <span data-feather="pocket"></span>
      {{ $questions }} Total Questions
    </div>
  </div>

  <div class="col-sm-3">
    <div style="border: 1px solid #ccc; padding: 40px 25px;width: 100%;" class="alert alert-success">
      <span data-feather="check-circle"></span>
      {{ $results }} Total Games Played
    </div>
  </div>

  <div class="col-sm-3">
    <div style="border: 1px solid #ccc; padding: 40px 25px;width: 100%;" class="alert alert-primary">
      <span data-feather="briefcase"></span>
      {{ $quizes }} Total Quizez in Quizat
    </div>
  </div>

  <div class="col-sm-3">
    <a class="nav-link" href="/statistics">
      <div style="border: 1px solid #ccc; padding: 40px 25px;width: 100%;" class="alert alert-link">
        <span data-feather="activity"></span>
         Question Statistics              
      </div>
    </a>
  </div>

 </div>
</main>

<script>
var i = 0;
var txt = "Welcome Back! {{ auth()->user()->name }} to Our Admin's Area";
var speed = 50;

function typeWriter() {
  if (i < txt.length) {
    document.getElementById("demo").innerHTML += txt.charAt(i);
    i++;
    setTimeout(typeWriter, speed);
  }
}
</script>


</body>
@endsection