@extends('dashboard.masterAdmin')
@section('admin')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
	<h2>All Categories</h2>
	<hr>
@if($flash = session('message'))
	<div class="alert alert-warning" role="alert">
		<b>{{ $flash }}</b>
	</div>	
@endif
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<table class="table table-hover">
			<tr>
				<th>Id</th>
				<th>Name</th>
				<th>Image</th>
				<th>Description</th>
				<th>View Questions</th>
				<th>Edit</th>
				<th>Delete</th>
			</tr>
				@foreach($categories as $category)
			<tr>
				<td>{{ $category->id }}</td>
				<td>{{ $category->name }}</td>
				<td><img src="/public/uploads/{{ $category->image }}" width="75"></td>
				<td>{{ $category->describtion }}</td>
				<td><a href="/allQ/{{ $category->id }}"><button class="btn btn-warning">View Questions</button></a></td>
				<td><a href="/editCat/{{ $category->id }}"><button class="btn btn-success">Edit</button></a></td>
				<td><a href="/deleteCat/{{ $category->id }}"><button class="btn btn-danger">Delete</button></a></td>
			</tr>
				@endforeach			
		</table>
	</div>
	<hr>
	<hr>
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
	<div class="container">
  <form method="POST" action="/creaCat" enctype="multipart/form-data">
  	{{ csrf_field() }}
    <div class="form-group">
      <label for="name" class="col-sm-2 col-form-label"><b>Quiz Name</b></label>
        <input type="text" name='name' class="form-control" id="name" placeholder="Name">
    </div>
    <div class="form-group">
			<label for="upload" class="col-sm-2 col-form-label"><b>Upload Image</b></label>
			<input type="file" name="image" class="form-control">
	</div>
	<div class="form-group">
	<label for="body" class="col-sm-2 col-form-label"><b>Description</b></label>
	<textarea name="describtion" id="describtion" class="form-control" rows="7" cols="10" style="width: 500px"></textarea>
	</div>
    <div class="form-group row">
      <div class="offset-sm-2 col-sm-10">
        <button type="submit" class="btn btn-info">Add Question</button>
      </div>
    </div>
    @include("errors.errors")
  </form>
</div>
</div>
</main>
@endsection