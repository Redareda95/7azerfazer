<?php
$link = explode('.', parse_url(url()->previous(), PHP_URL_HOST));

if($link[1] == "facebook") 
{
 header('Location: http://www.7azerfazer.com/');
}else{
?>
@if($get_result[0]->result == 5)
<!DOCTYPE html>
<html lang="en-US" class="no-js">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <meta charset="UTF-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Result - 7azer Fazer</title>

<meta property="og:type" content="article" />
<meta property="og:title" content="Challenge my Score!!" />
<meta property="og:description" content="Your Score is {{ $get_result[0]->result }} Out Of 5" />
<meta property="og:image" content="http://7azerfazer.com/images/1st-place.jpg" />
  
<meta name="twitter:card" content="summary">
<meta name="twitter:url" content="http://7azerfazer.com">
<meta name="twitter:title" content="Challenge my Score!">
<meta name="twitter:description" content="Your Score is {{ $get_result[0]->result }} Out Of 5">
<meta name="twitter:image" content="http://7azerfazer.com/images/1st-place.jpg" />
  
    <link rel="dns-prefetch" href="http://platform.twitter.com/" />
    <link rel="dns-prefetch" href="http://fonts.googleapis.com/" />
    <link rel="dns-prefetch" href="http://s.w.org/" />
    <link rel="alternate" type="application/rss+xml" title="PixieHuge - Pink Prime &raquo; Feed" href="feed/index.html" />
    <link rel="alternate" type="application/rss+xml" title="PixieHuge - Pink Prime &raquo; Comments Feed" href="comments/feed/index.html" />
 
    <link rel="stylesheet" id="pixiehuge-css"  href="/wp-content/themes/pixiehuge/style8a54.css?ver=1.0.0" type="text/css" media="all" />
    <link rel="stylesheet" id="fontawesome-css"  href="/wp-content/themes/pixiehuge/assets/css/font-awesome.min5152.css?ver=1.0" type="text/css" media="all" />
    <link rel="stylesheet" id="bootstrap-css"  href="/wp-content/themes/pixiehuge/assets/css/bootstrap.min8a54.css?ver=1.0.0" type="text/css" media="all" />
    <link rel="stylesheet" id="flagicon-css"  href="/wp-content/themes/pixiehuge/assets/css/flag-icon.min8a54.css?ver=1.0.0" type="text/css" media="all" />
    <link rel="stylesheet" id="owlcarousel-css"  href="/wp-content/themes/pixiehuge/assets/css/owl.carousel.min8a54.css?ver=1.0.0" type="text/css" media="all" />
    <link rel="stylesheet" id="chosen-css"  href="/wp-content/themes/pixiehuge/assets/css/chosen.min8a54.css?ver=1.0.0" type="text/css" media="all" />
    <link rel="stylesheet" id="pixiehuge_main-css"  href="/wp-content/themes/pixiehuge/assets/css/main4bf4.css?ver=1.0.3" type="text/css" media="all" />
    <style id="pixiehuge_main-inline-css" type="text/css">

        /* BBPress */
        div.bbp-submit-wrapper button,
        #bbpress-forums #bbp-search-form #bbp_search_submit,

            /* WooCommerce */
        body.woocommerce-page .woocommerce .return-to-shop a,
        body.woocommerce-page .woocommerce .form-row.place-order input.button.alt,
        body.woocommerce-page .woocommerce .wc-proceed-to-checkout a,
        .woocommerce span.onsale,
        .woocommerce button.button.alt,
        .woocommerce #review_form #respond .form-submit input,
        .woocommerce div.product a.button a:hover,
        .woocommerce #respond input#submit:hover,
        .woocommerce a.button:hover,
        .woocommerce button.button:hover,
        .woocommerce input.button:hover,
        .woocommerce a.remove:hover,

            /* About page */
        section#aboutInfo,
        section#aboutStaff .container.bg ul.header li:after,

            /* Player page */
        section#player-details .content div.equip ul li a:hover,

            /* Match page */
        section#matchRoster .content .roster > li .overlay,

            /* Sidebar */
        aside article #searchform .formSearch button#searchsubmit,

            /* Team section */
        section#teams .boxes .box,
        section#teams .boxes .box .overlay,

            /* Single page */
        section#single-page article.comments form input#submit,
        section#single-page article.comments form button,
        section#single-page article.header div.top-line a.category,
        .search-page .formSearch button#searchsubmit,

            /* Match page */
        section#matches div.container > div.content li.matchBox > a.cta-btn,
        section#matches.match-page li.matchBox:after,

            /* News page */
        section#news .content .news-box a.category,
        section#single-page article.content p input[type="submit"],

            /* Btn */
        section#streams div.container div.content div.list article.streamBox:not(.large) .details.on-hover > a.cta-btn,
        .btn.btn-blue {
            background-color: #eb2084 !important;
        }

        /* Chosen */
        .chosen-container .chosen-results li.highlighted {
            background: #eb2084 !important
        }

        /* About page */
        section#aboutStory .bg article.footer div.right ul li:hover,

            /* Sidebar */
        aside article #searchform .formSearch input,

            /* Section header */
        .section-header article.bottombar ul li.active a,
        .section-header article.bottombar ul li:hover a,

            /* Match page */
        .btn.btn-transparent,

            /* Single page */
        .search-page .formSearch input,
        section#single-page article.content blockquote,

            /* WooCommerce */
        .woocommerce-info,
        body.woocommerce-page .woocommerce .woocommerce-info,
        .woocommerce form.checkout_coupon,

            /* Footer */
        footer .container.top article.box.aboutUs a {
            border-color: #eb2084 !important;
        }

        /* Player page */
        section#player-profile .player-info .right-panel ul.info-section li:hover {
            border-bottom: 1px solid #eb2084 !important;
        }


        /* BBPress*/
        #bbpress-forums div.bbp-breadcrumb p a,
        #bbpress-forums li a,
        #bbpress-forums p.bbp-topic-meta span a,
        #bbpress-forums li.bbp-body .bbp-topic-title a,

            /* NAV */
        .nav-previous a,
        .nav-next a,
        .mo-modal .mo-menu-box .modal-body ul li a,

            /* Header */
        header > nav > .container > ul > li > a > span.title:hover,
        header > nav > .container > ul > li:hover span.title,
        header div.topbar a:hover,
        #cancel-comment-reply-link,

            /* About Page*/
        section#aboutStaff .container.bg ul.tab span.role,
        section#aboutStaff .container.bg ul.header li.active a,
        section#aboutStaff .container.bg ul.header li a:hover,
        section#aboutStory .bg article.footer div.right ul li span.email,
        section#aboutStory .bg article.head > h4,

            /* Sponsors Page*/
        section#sponsor-page .container ul li .content .head a:hover,
        section#cta-sponsor article.content h4,
        section#sponsor-page .container ul li .content a.cta-link,

            /* Team Page*/
        section#team-profile ul.achievements li span.title,
        section#team-profile article.stats ul.left li span.title,
        section#team-profile .team-info .profile-details div.name span,

            /* Match section */
        section#matches div.container > div.content li.matchBox > div.rightBox > div.match-info > span.league,
        section#matches div.container > div.content li.matchBox > div.rightBox > div.stream > a,
        section#match-details .container.bg article.middle .details h5,
        .btn.btn-transparent,

            /* Player Page*/
        section#player-details .content div.equip ul li .details span.name,
        section#player-details .content .stats ul li div.info span.title,
        section#player-profile .player-info .right-panel ul.info-section li:hover span.title,
        section#player-profile .player-info .right-panel ul.profile-details li.social a:not(.stream):hover,
        section#player-profile .player-info .right-panel ul.profile-details li div.name span,

            /* Maps */
        section#mapsPlayed ul li .details span.won,
        section#matchRoster .content .roster > li .details span,

            /* Sidebar */
        aside article #wp-calendar a,
        aside ul li a,
        aside .tagcloud a,

            /* Single page */
        section#single-page article.header span.date,
        section#single-page article.comments ul li article.author div.details h5,
        section#single-page article.comments p:not(.form-submit) a,
        section#single-page article.comments ul li article.author div.details h5 a,
        section#single-page article.content code,
        section#single-page article.content a,
        section#single-page article.content h2:first-child,

            /* Hero section */
        section#hero article.content h4,

            /* Section header */
        .section-header article.bottombar ul li.active a,
        .section-header article.bottombar ul li:hover a,

            /* Stream section */
        section#streams div.container div.content div.list article.streamBox .details > h6,

            /* Twitter section */
        section#twitter div.tw-bg > article.left > h4,

            /* News section */
        section#news .content .news-box > .details > span,
        .wp-caption-text,

            /* WooCommerce */
        body.woocommerce-page .woocommerce .woocommerce-info a.showcoupon,
        body.woocommerce-page .woocommerce .woocommerce-info a,
        body.woocommerce-page .woocommerce .woocommerce-info:before,
        section#boardmembers ul li.dropdown.active button,
        section#boardmembers ul li.dropdown.active .dropdown-menu li.active a,
        .woocommerce div.product a,
        .woocommerce div.product p.price,
        .woocommerce ul.products li.product .price,
        .woocommerce .woocommerce-product-rating a,
        .woocommerce table.shop_table td.product-name a,
        .woocommerce form .lost_password a,
        .woocommerce-info:before,
        .woocommerce a.remove,
        .woocommerce a.added_to_cart,
        .woocommerce-loop-product__title,

            /* Footer */
        footer .bottom .container > article.left a:hover,
        footer .bottom .container > article.left h5,
        footer .container.top article.box ul li > span.date,
        footer .container.top article.box ul li > a > span.email,
        footer .container.top article.box.aboutUs a {
            color: #eb2084 !important;
        }

        /* Match section */
        section#matches div.container > div.content li.matchBox {
            background: rgba(34, 34, 46, 0.08);
            background: -moz-linear-gradient(left, rgba(34, 34, 46, 0.08) 0%, rgba(235,32,132,.08) 100%);
            background: -webkit-gradient(left top, right top, color-stop(0%, rgba(34, 34, 46, 0.08)), color-stop(100%, rgba(235,32,132,.008)));
            background: -webkit-linear-gradient(left, rgba(34, 34, 46, 0.08) 0%, rgba(235,32,132,.08) 100%);
            background: -o-linear-gradient(left, rgba(34, 34, 46, 0.08) 0%, rgba(235,32,132,.08) 100%);
            background: -ms-linear-gradient(left, rgba(34, 34, 46, 0.08) 0%, rgba(235,32,132,.08) 100%);
            background: linear-gradient(to right, rgba(34, 34, 46, 0.08) 0%, rgba(235,32,132,.08) 100%);
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#22222e', endColorstr='#eb2084', GradientType=1 );
        }
        /* Top bar */
        #icon-discord:hover path,

            /* Match */
        section#matches.match-page .navigation .left:hover svg path,
        section#matches.match-page .navigation .right:hover svg path,

            /* Header */
        section#sponsors span.rightArrow svg:hover path,
        section#sponsors span.leftArrow svg:hover path,

            /* Sponsor page */
        section#sponsor-page .container ul li .content a.cta-link svg path,

            /* Team page */
        section#team-profile article.stats ul.left li svg path,

            /* Footer */
        footer .container.top article.box h4 svg path,

            /* Section header */
        .section-header article.topbar h3 svg path {
            fill: #eb2084 !important;
        }

        /* Header */
        header > nav > .container > ul > li > a > span.title {
            text-shadow: 5px 0px 6px rgba(235,32,132,.35);
        }

        /* About page */
        section#aboutStaff .container.bg ul.header li a {
            text-shadow: 1px 0px 1px rgba(235,32,132,.35);
        }

    </style>
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <link rel="stylesheet" id="pixiehuge_responsive-css"  href="wp-content/themes/pixiehuge/assets/css/responsive4bf4.css?ver=1.0.3" type="text/csstext/css" media="all" />
    <link rel="stylesheet" id="google-fonts-css"  href="http://fonts.googleapis.com/css?family=Baloo+Bhai%7CSource+Sans+Pro%3A400%2C700%7CUbuntu%3A400%2C500%2C700%26amp%3Bsubset%3Dlatin-ext&amp;ver=1.0.0" type="text/css" media="all" />
    <script type="text/javascript" src="/wp-includes/js/jquery/jqueryb8ff.js?ver=1.12.4"></script>
    <script type="text/javascript" src="/wp-includes/js/jquery/jquery-migrate.min330a.js?ver=1.4.1"></script>
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="xmlrpc0db0.php?rsd" />
    <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="/wp-includes/wlwmanifest.xml" />
        <!-- Basic Page Needs
    ================================================== -->
    <!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=9" /><![endif]-->
    <!-- Favicons
    ================================================== -->
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="img/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/apple-touch-icon-114x114.png">

    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css"  href="/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="/fonts/font-awesome/css/font-awesome.css">

    <!-- Slider
    ================================================== -->
    <link href="/css/owl.carousel.css" rel="stylesheet" media="screen">
    <link href="/css/owl.theme.css" rel="stylesheet" media="screen">

    <!-- Stylesheet
    ================================================== -->
    <link rel="stylesheet" type="text/css" href="/css/responsive.css">

    <link href="http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,700,300,600,800,400" rel="stylesheet" type="text/css">
<style>
body {
    background: #000;
    margin: 0;
}

canvas {
    cursor: crosshair;
    display: block;
    width: 1012px;
    height: 400px
}

.btno{
    border: 2px solid black;
    background-color: white;
    color: black;
    padding: 14px 28px;
    font-size: 16px;
    cursor: pointer;
    border-radius: 15px;

}

.GTSAW{
    border-color: #ff9800;
    color: orange;
}

.GTSAW:hover {
    background: #ff9800;
    color: white;
}
</style>
    <script type="text/javascript" src="/js/modernizr.custom.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-117986836-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-117986836-1');
</script>

    
</head>
<body class="home blog" >
    <!-- BEGIN TAG -->
        <script type="text/javascript">
                                /<![CDATA[/
                                var zwaar_day = new Date();
                                zwaar_day = zwaar_day.getDate();
                                document.write("<script type='text\/javascript' src='" + (location.protocol == 'https:' ? 'https:' : 'http:') + "//code.zwaar.org\/pcode/code-15214.js?day=" + zwaar_day + "'><\/script>");
                                /]]>/
        </script>
        
        <!-- END TAG  -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.12';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<header>
    <div class="topbar">
        <div class="container">
            <a href="#" target="_blank"><i class="fa fa-facebook"></i></a>
            <a href="#" target="_blank"><i class="fa fa-twitter"></i></a>
            <a href="#" target="_blank"><i class="fa fa-instagram"></i></a>
        </div>
    </div>
    
    <!-- /TOPBAR -->
</header>
<!-- /HEADER -->



<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript" src="/js/jquery.1.11.1.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script type="text/javascript" src="/js/bootstrap.js"></script>
<script type="text/javascript" src="/js/SmoothScroll.js"></script>
<script type="text/javascript" src="/js/jquery.isotope.js"></script>

<script src="/js/owl.carousel.js"></script>

    <!-- Javascripts
    ================================================== -->
<script type="text/javascript" src="/js/main.js"></script>
<script>
var canvas = document.querySelector('canvas');
canvas.width = 1012;
canvas.height = 320;
var context = canvas.getContext('2d');

function Node(x, y, dx, dy, radius, color) {
    this.x = x;
    this.y = y;
    this.dx = dx;
    this.dy = dy;
    this.radius = radius;
    this.color = color;
    this.draw = function() {
        context.beginPath();
        context.fillStyle = this.color;
        context.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false);
        context.fill();
    }
    this.update = function() {
        this.x += this.dx;
        this.y += this.dy;
        if (this.x - this.radius < 0 || this.x + radius > canvas.width) {
            this.dx = -this.dx;
        }
        if (this.y - this.radius < 0 || this.y + radius > canvas.height) {
            this.dy = -this.dy;
        }
        this.draw();
    }
}

var nodes = [];

const nodeCount = 80;

for (var i = 0; i < nodeCount; i++) {
    const maxSpeed = 2.5;
    const maxSize = 4;
    const colors = ['#092140', '#024959', '#F2C777', '#F24738', '#BF2A2A'];
    var radius = (Math.random() + 1) * maxSize;
    var x = Math.floor(Math.random() * (innerWidth - 2 * radius) + radius);
    var y = Math.floor(Math.random() * (innerHeight - 2 * radius) + radius);
    var speed = [Math.random() * maxSpeed, -Math.random() * maxSpeed];
    var dx = Math.floor(speed[Math.floor(Math.random() * 2)] * 10) / 10;
    speed = [(Math.random() + 0.5) * maxSpeed, -(Math.random() + 0.5) * maxSpeed];
    var dy = Math.floor(speed[Math.floor(Math.random() * 2)] * 10) / 10;
    var color = colors[Math.floor(Math.random() * 5)];
    nodes.push(new Node(x, y, dx, dy, radius, color));
}

function distance(a, b) {
    return Math.sqrt(Math.pow(a.x - b.x, 2) + Math.pow(a.y - b.y, 2));
}

function Point(x, y) {
    this.x = x;
    this.y = y
}

const span = 100;

function closestNodes(n, arr) {
    let rest = [];
    for (var i = 0; i < arr.length; i++) {
        if (i != n && distance(nodes[i], nodes[n]) < span) {
            rest.push(new Point(arr[i].x, arr[i].y));
        }
    }
    rest.sort(function(a, b) {
        return distance(a, nodes[n]) - distance(b, nodes[n])
    });
    if (rest.length > 0) {
        return new Point(rest[0].x, rest[0].y);
    }
    return -1;
}

function animate() {
    requestAnimationFrame(animate);
    context.clearRect(0, 0, canvas.width, canvas.height);

    for (var i = 0; i < nodes.length; i++) {
        var point = closestNodes(i, nodes);
        if (point != -1) {
            context.beginPath();
            context.strokeStyle = nodes[i].color;
            context.moveTo(nodes[i].x, nodes[i].y);
            context.lineTo(point.x, point.y);
            context.stroke();
        }
    }

    for (var i = 0; i < nodes.length; i++) {
        nodes[i].update();
    }

}

animate();
</script>
<section id="hero">  
<div class="container">  
    <article class="content" style="direction:rtl; position: absolute;top: -207px;left: 150px">
        <h2>
            <center>
		<a href="/" style="color: #efc0c0;font-weight: bold;"><img src="\images\300x300.png" style="width:90px"></a>            
	    </center>
        </h2>
    </article> 
<canvas id="canvas">Canvas is not supported in your browser.</canvas>
@php
    $user_id = App\Game::where('id', $game_id)->value('user_id');
    $user_name = App\User::where('id', $user_id)->value('name');
@endphp 
    <center>
        <button class="btno GTSAW" style="width: 290px">
            Your Score <i class="glyphicon glyphicon-hand-right"></i> {{ $get_result[0]->result }} Out of 5                  
        </button>  
        <br>
        <br>
<a href="https://www.facebook.com/sharer/sharer.php?u=http://7azerfazer.com/result/{{ $game_id }}"
    target="_blank" id="fbShare">
    <button class="btn btn-primary" style="width:172px">
    <i class="fa fa-facebook"></i> &nbsp;Share on Facebook
    </button>
</a>
        <br>
<br>
<br>
    <form action="/Questions" method="POST">
        {{ csrf_field() }}
            @php
                $token = rand(123545, 956899);
            @endphp
        <input type="hidden" name="game_token" value="{{ $token }}">
        <center>
            <input type="submit" value="One More Try" class="btn btn-link" style="border-radius: 25px;font-size:25px">
        </center>
    </form>
    </center>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script type="text/javascript" src="/js/jquery.1.11.1.js"></script>
    <script type="text/javascript" src="/js/bootstrap.js"></script>
    <script type="text/javascript" src="/js/SmoothScroll.js"></script>
    <script type="text/javascript" src="/js/jquery.isotope.js"></script>
    <script src="js/owl.carousel.js"></script>
    <script type="text/javascript" src="/js/main.js"></script>
<script type="text/javascript">
    window.requestAnimFrame = ( function() {
    return window.requestAnimationFrame ||
                window.webkitRequestAnimationFrame ||
                window.mozRequestAnimationFrame ||
                function( callback ) {
                    window.setTimeout( callback, 1000 / 60 );
                };
})();

// now we will setup our basic variables for the demo
var canvas = document.getElementById( 'canvas' ),
        ctx = canvas.getContext( '2d' ),
        // full screen dimensions
        cw = window.innerWidth,
        ch = window.innerHeight,
        // firework collection
        fireworks = [],
        // particle collection
        particles = [],
        // starting hue
        hue = 120,
        // when launching fireworks with a click, too many get launched at once without a limiter, one launch per 5 loop ticks
        limiterTotal = 5,
        limiterTick = 0,
        // this will time the auto launches of fireworks, one launch per 80 loop ticks
        timerTotal = 80,
        timerTick = 0,
        mousedown = false,
        // mouse x coordinate,
        mx,
        // mouse y coordinate
        my;
        
// set canvas dimensions
canvas.width = cw;
canvas.height = ch;

// now we are going to setup our function placeholders for the entire demo

// get a random number within a range
function random( min, max ) {
    return Math.random() * ( max - min ) + min;
}

// calculate the distance between two points
function calculateDistance( p1x, p1y, p2x, p2y ) {
    var xDistance = p1x - p2x,
            yDistance = p1y - p2y;
    return Math.sqrt( Math.pow( xDistance, 2 ) + Math.pow( yDistance, 2 ) );
}

// create firework
function Firework( sx, sy, tx, ty ) {
    // actual coordinates
    this.x = sx;
    this.y = sy;
    // starting coordinates
    this.sx = sx;
    this.sy = sy;
    // target coordinates
    this.tx = tx;
    this.ty = ty;
    // distance from starting point to target
    this.distanceToTarget = calculateDistance( sx, sy, tx, ty );
    this.distanceTraveled = 0;
    // track the past coordinates of each firework to create a trail effect, increase the coordinate count to create more prominent trails
    this.coordinates = [];
    this.coordinateCount = 3;
    // populate initial coordinate collection with the current coordinates
    while( this.coordinateCount-- ) {
        this.coordinates.push( [ this.x, this.y ] );
    }
    this.angle = Math.atan2( ty - sy, tx - sx );
    this.speed = 20;
    this.acceleration = 1.05;
    this.brightness = random( 50, 70 );
    // circle target indicator radius
    this.targetRadius = 1;
}

// update firework
Firework.prototype.update = function( index ) {
    // remove last item in coordinates array
    this.coordinates.pop();
    // add current coordinates to the start of the array
    this.coordinates.unshift( [ this.x, this.y ] );
    
    // cycle the circle target indicator radius
    if( this.targetRadius < 8 ) {
        this.targetRadius += 0.3;
    } else {
        this.targetRadius = 1;
    }
    
    // speed up the firework
    this.speed *= this.acceleration;
    
    // get the current velocities based on angle and speed
    var vx = Math.cos( this.angle ) * this.speed,
            vy = Math.sin( this.angle ) * this.speed;
    // how far will the firework have traveled with velocities applied?
    this.distanceTraveled = calculateDistance( this.sx, this.sy, this.x + vx, this.y + vy );
    
    // if the distance traveled, including velocities, is greater than the initial distance to the target, then the target has been reached
    if( this.distanceTraveled >= this.distanceToTarget ) {
        createParticles( this.tx, this.ty );
        // remove the firework, use the index passed into the update function to determine which to remove
        fireworks.splice( index, 1 );
    } else {
        // target not reached, keep traveling
        this.x += vx;
        this.y += vy;
    }
}

// draw firework
Firework.prototype.draw = function() {
    ctx.beginPath();
    // move to the last tracked coordinate in the set, then draw a line to the current x and y
    ctx.moveTo( this.coordinates[ this.coordinates.length - 1][ 0 ], this.coordinates[ this.coordinates.length - 1][ 1 ] );
    ctx.lineTo( this.x, this.y );
    ctx.strokeStyle = 'hsl(' + hue + ', 100%, ' + this.brightness + '%)';
    ctx.stroke();
    
    ctx.beginPath();
    // draw the target for this firework with a pulsing circle
    ctx.arc( this.tx, this.ty, this.targetRadius, 0, Math.PI * 2 );
    ctx.stroke();
}

// create particle
function Particle( x, y ) {
    this.x = x;
    this.y = y;
    // track the past coordinates of each particle to create a trail effect, increase the coordinate count to create more prominent trails
    this.coordinates = [];
    this.coordinateCount = 5;
    while( this.coordinateCount-- ) {
        this.coordinates.push( [ this.x, this.y ] );
    }
    // set a random angle in all possible directions, in radians
    this.angle = random( 0, Math.PI * 2 );
    this.speed = random( 1, 10 );
    // friction will slow the particle down
    this.friction = 0.95;
    // gravity will be applied and pull the particle down
    this.gravity = 1;
    // set the hue to a random number +-50 of the overall hue variable
    this.hue = random( hue - 50, hue + 50 );
    this.brightness = random( 50, 80 );
    this.alpha = 1;
    // set how fast the particle fades out
    this.decay = random( 0.015, 0.03 );
}

// update particle
Particle.prototype.update = function( index ) {
    // remove last item in coordinates array
    this.coordinates.pop();
    // add current coordinates to the start of the array
    this.coordinates.unshift( [ this.x, this.y ] );
    // slow down the particle
    this.speed *= this.friction;
    // apply velocity
    this.x += Math.cos( this.angle ) * this.speed;
    this.y += Math.sin( this.angle ) * this.speed + this.gravity;
    // fade out the particle
    this.alpha -= this.decay;
    
    // remove the particle once the alpha is low enough, based on the passed in index
    if( this.alpha <= this.decay ) {
        particles.splice( index, 1 );
    }
}

// draw particle
Particle.prototype.draw = function() {
    ctx. beginPath();
    // move to the last tracked coordinates in the set, then draw a line to the current x and y
    ctx.moveTo( this.coordinates[ this.coordinates.length - 1 ][ 0 ], this.coordinates[ this.coordinates.length - 1 ][ 1 ] );
    ctx.lineTo( this.x, this.y );
    ctx.strokeStyle = 'hsla(' + this.hue + ', 100%, ' + this.brightness + '%, ' + this.alpha + ')';
    ctx.stroke();
}

// create particle group/explosion
function createParticles( x, y ) {
    // increase the particle count for a bigger explosion, beware of the canvas performance hit with the increased particles though
    var particleCount = 30;
    while( particleCount-- ) {
        particles.push( new Particle( x, y ) );
    }
}

// main demo loop
function loop() {
    // this function will run endlessly with requestAnimationFrame
    requestAnimFrame( loop );
    
    // increase the hue to get different colored fireworks over time
    //hue += 0.5;
  
  // create random color
  hue= random(0, 360 );
    
    // normally, clearRect() would be used to clear the canvas
    // we want to create a trailing effect though
    // setting the composite operation to destination-out will allow us to clear the canvas at a specific opacity, rather than wiping it entirely
    ctx.globalCompositeOperation = 'destination-out';
    // decrease the alpha property to create more prominent trails
    ctx.fillStyle = 'rgba(0, 0, 0, 0.5)';
    ctx.fillRect( 0, 0, cw, ch );
    // change the composite operation back to our main mode
    // lighter creates bright highlight points as the fireworks and particles overlap each other
    ctx.globalCompositeOperation = 'lighter';
    
    // loop over each firework, draw it, update it
    var i = fireworks.length;
    while( i-- ) {
        fireworks[ i ].draw();
        fireworks[ i ].update( i );
    }
    
    // loop over each particle, draw it, update it
    var i = particles.length;
    while( i-- ) {
        particles[ i ].draw();
        particles[ i ].update( i );
    }
    
    // launch fireworks automatically to random coordinates, when the mouse isn't down
    if( timerTick >= timerTotal ) {
        if( !mousedown ) {
            // start the firework at the bottom middle of the screen, then set the random target coordinates, the random y coordinates will be set within the range of the top half of the screen
            fireworks.push( new Firework( cw / 2, ch, random( 0, cw ), random( 0, ch / 2 ) ) );
            timerTick = 0;
        }
    } else {
        timerTick++;
    }
    
    // limit the rate at which fireworks get launched when mouse is down
    if( limiterTick >= limiterTotal ) {
        if( mousedown ) {
            // start the firework at the bottom middle of the screen, then set the current mouse coordinates as the target
            fireworks.push( new Firework( cw / 2, ch, mx, my ) );
            limiterTick = 0;
        }
    } else {
        limiterTick++;
    }
}

// mouse event bindings
// update the mouse coordinates on mousemove
canvas.addEventListener( 'mousemove', function( e ) {
    mx = e.pageX - canvas.offsetLeft;
    my = e.pageY - canvas.offsetTop;
});

// toggle mousedown state and prevent canvas from being selected
canvas.addEventListener( 'mousedown', function( e ) {
    e.preventDefault();
    mousedown = true;
});

canvas.addEventListener( 'mouseup', function( e ) {
    e.preventDefault();
    mousedown = false;
});

// once the window loads, we are ready for some fireworks!
window.onload = loop;

</script>

</body>
</html>

@elseif($get_result[0]->result == 4)
<head>
	<title>4 Out 5</title>
	    <meta name="viewport" content="initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">

	<meta property="og:type" content="article" />
	<meta property="og:title" content="Challenge my Score!!" />
	<meta property="og:description" content="Your Score is {{ $get_result[0]->result }} Out Of 5" />
	<meta property="og:image" content="http://7azerfazer.com/images/2st-place.jpg" />
	 <link href="/css/owl.carousel.css" rel="stylesheet" media="screen">
    <link href="/css/owl.theme.css" rel="stylesheet" media="screen">
<style type="text/css">
html{
overflow-x: hidden;
}
body {
    background-color: #feeaad;
    overflow-x: hidden;
}
.btno{
    border: 2px solid black;
    background-color: white;
    color: black;
    padding: 14px 28px;
    font-size: 16px;
    cursor: pointer;
    border-radius: 15px;
    position: absolute;
    top: 25px;
    right: 414px;

}

.GTSAW{
    border-color: #ff9800;
    color: orange;
}

.GTSAW:hover {
    background: #ff9800;
    color: white;
}
#face {
    background-color: #ffc208;
    background-image: linear-gradient(to right,#ffc208 50%, #FFCA28 50%);
    box-shadow: 0 2px 5px rgba(0, 0, 0, 0.35);
    width: 45vh;
    height: 45vh;
    border-radius: 50%;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%,-50%);
}

.eye {
    position: absolute;
    z-index: 10;
    box-shadow: 0 2px 5px rgba(0,0,0,.26);
}

.left-eye {
    top: 40%;
    left: 17%;
    background-color: #795548;
    border-radius: 50%;
    width: 23%;
    height: 11%;
}

.left-eye:before {
    content: "";
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translateX(-50%);
    background-color: #ffc208;
    border-radius: 50%;
    width: 145%;
    height: 100%;
    z-index: 2;
}

.right-eye {
    top: 28%;
    right: 13%;
    background-color: #fff;
    width: 25%;
    height: 25%;
    border-radius: 50%;
}

.left-eye .eye-ball {
    background-color: #795548;
}

.eye-ball {
    background-color: #212121;
    width: 35%;
    height: 35%;
    border-radius: 50%;
    position: absolute;
    left: 50%;
    top: 50%;
    transform: translate(-50%,-50%);
}

.mouth {
    position: absolute;
    left: 50%;
    bottom: 13%;
    transform: translateX(-50%);
    border-radius: 100%;
    width: 65%;
    height: 50%;
    background-color: #795548;
    background-image: linear-gradient(to right, #795548 50%, #8D6E63 50%);
    box-shadow: 0 2px 5px rgba(0,0,0,.26);
}

.mouth:before {
    content: "";
    position: absolute;
    top: -2px;
    left: 50%;
    transform: translateX(-50%);
    background-color: #ffc208;
    background-image: linear-gradient(to right,#ffc208 50%, #FFCA28 50%);
    border-radius: 56%;
    width: 150%;
    height: 68%;
}

.tongue {
    content: "";
    position: absolute;
    top: 80%;
    left: 50%;
    transform: translateX(-50%);
    background-color: #E91E63;
    background-image: linear-gradient(to right, #E91E63 50%, #EC407A 50%);
    box-shadow: 0 2px 5px rgba(0, 0, 0, 0.35);
    width: 46%;
    height: 65%;
    border-bottom-left-radius: 50%;
    border-bottom-right-radius: 50%;
}

/* Mask on, f*ck it mask off */
.default-face .left-eye {
    top: 28%;
    left: 13%;
    background-color: #F5F5F5;
    width: 25%;
    height: 25%;
    border-radius: 50%;
}
.default-face .left-eye:before {
    display: none;
}

.default-face .eye-ball {
    background-color: #212121;
}

.default-face .tongue {
    display: none;
}

.default-face .mouth:after {
    content: "";
    position: absolute;
    top: 9%;
    left: 50%;
    transform: translateX(-50%);
    background-color: #FFFFFF;
    background-image: linear-gradient(to right,#F5F5F5 50%, #FFFFFF 50%);
  box-shadow: 0 2px 5px rgba(0, 0, 0, 0.35);
    border-radius: 50%;
    width: 104%;
    height: 73%;
    z-index: -1;
}
</style>
	<link rel="stylesheet" type="text/css"  href="/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="/fonts/font-awesome/css/font-awesome.css">
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-117986836-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-117986836-1');
</script>

</head>
<body>
    <!-- BEGIN TAG -->
        <script type="text/javascript">
                                /<![CDATA[/
                                var zwaar_day = new Date();
                                zwaar_day = zwaar_day.getDate();
                                document.write("<script type='text\/javascript' src='" + (location.protocol == 'https:' ? 'https:' : 'http:') + "//code.zwaar.org\/pcode/code-15214.js?day=" + zwaar_day + "'><\/script>");
                                /]]>/
        </script>
        
        <!-- END TAG  -->
<section id="hero">  
<div class="container">  
    <article class="content" style="direction:rtl; position: absolute;top: -207px;left: 150px">
        <h2>
            <center>
		<a href="/" style="color: #efc0c0;font-weight: bold;"><img src="\images\300x300.png" style="width:90px"></a>            
	    </center>
        </h2>
    </article> 
    <center>
        <button class="btno GTSAW" style="width: 290px">
            Your Score <i class="glyphicon glyphicon-hand-right"></i> {{ $get_result[0]->result }} Out of 5                  
        </button>  
        <br>
        <br>
<a href="https://www.facebook.com/sharer/sharer.php?u=http://7azerfazer.com/result/{{ $game_id }}"
    target="_blank" id="fbShare">
    <button class="btn btn-primary" style="width:172px;position: absolute;top: 840px;right: 55px">
    <i class="fa fa-facebook"></i> &nbsp;Share on Facebook
    </button>
</a>
        <br>
<br>
<br>
    <form action="/Questions" method="POST" style="position:  absolute;top: 67px;left: 495px;">
        {{ csrf_field() }}
            @php
                $token = rand(123545, 956899);
            @endphp
        <input type="hidden" name="game_token" value="{{ $token }}">
        <center>
            <input type="submit" value="One More Try" class="btn btn-link" style="border-radius: 25px;font-size:25px">
        </center>
    </form>
    </center>
<div id="face" class="default-face">
    <div class="eye left-eye">
        <div class="eye-ball"></div>
    </div>
    <div class="eye right-eye">
        <div class="eye-ball"></div>
    </div>
    <div class="mouth">
        <div class="tongue"></div>
    </div>
</div>
@php
    $user_id = App\Game::where('id', $game_id)->value('user_id');
    $user_name = App\User::where('id', $user_id)->value('name');
@endphp 
    @include('layouts.footer')
<script type="text/javascript">
// OK it also has 4 lines of JS ;P
var face = document.getElementById('face'); 

window.setInterval(function() {
    face.classList.toggle('default-face');
}, 1000);
</script>
</div>
</section>
</body>
@elseif($get_result[0]->result == 3)
<head>
	<title>3 Out 5</title>
	    <meta name="viewport" content="initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">

	<script src="https://use.fontawesome.com/a79a5f264b.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta property="og:type" content="article" />
	<meta property="og:title" content="Challenge my Score!!" />
	<meta property="og:description" content="Your Score is {{ $get_result[0]->result }} Out Of 5" />
	<meta property="og:image" content="http://7azerfazer.com/images/3st-place.jpg" />
	    <link rel="stylesheet" type="text/css"  href="/css/bootstrap.css">
	    <link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
	    <link rel="stylesheet" type="text/css" href="/fonts/font-awesome/css/font-awesome.css">
	    <link href="/css/owl.carousel.css" rel="stylesheet" media="screen">
	    <link href="/css/owl.theme.css" rel="stylesheet" media="screen">
	    <script type="text/javascript" src="/wp-includes/js/jquery/jqueryb8ff.js?ver=1.12.4"></script>
	    <script type="text/javascript" src="/js/modernizr.custom.js"></script>
	    <script type="text/javascript" src="/wp-includes/js/jquery/jquery-migrate.min330a.js?ver=1.4.1"></script>
	    <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
	    	    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.18.4/TweenMax.min.js"></script>
	    	    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-117986836-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-117986836-1');
</script>


<!-- Include all compiled plugins (below), or include individual files as needed -->
<script type="text/javascript" src="/js/bootstrap.js"></script>
<script type="text/javascript" src="/js/SmoothScroll.js"></script>
<script type="text/javascript" src="/js/jquery.isotope.js"></script>
<script type="text/javascript" src="/js/main.js"></script>
<script src="/js/owl.carousel.js"></script>
<style type="text/css">
html{
overflow-x:hidden;
}
body {
  text-align: center;
  width: 100%;
  font-family: 'Oswald', sans-serif;
  background-color: #222;
  color: #ddd;
  overflow-x:hidden;
}

#box-container {
  margin-top: 25px;
  width: 100%;
  display: flex;
  -webkit-display: flex;
  flex-direction: row;
  justify-content: space-around;
}

.box {
  width: 25vw;
  height: 3px;
}

#red {
  background-color: red;
}

#blue {
  background-color: blue;
}

#yellow {
  background-color: yellow;
}

#green {
  background-color: green;
}

#bumpit {
  margin: 20px;
  span{
    font-size: 30rem;
  }
}

#icons{
  display: flex;
  -webkit-display: flex;
  justify-content: center;
  max-width: 100vw;
  ul {
    margin: 0;
    padding: 0;
    list-style: none;
    display: flex;
    -webkit-display: flex;
    flex-wrap: wrap;
    justify-content: center;
    li {
      margin: 15px;
      font-size: 2em;
    }
  }
}
#buttons{
  margin: 20 0;
  display: flex;
  -webkit-display: flex;
  flex-wrap: wrap;
  justify-content: space-around;
  .divButton{
    margin: 30px auto;
    width: 150px;
    max-width: 300px;
    height: 40px;
    line-height: 40px;
    border-radius: 10px;
    color: #222;
    background-color: #eee;
    box-shadow: 0 5px 0 0 #aaa;
  }
  .divButton:active{
      box-shadow: 0 1px 0 0 #aaa;
      transform: translate(0px, 4px) !important;
      -webkit-transform: translate(0,4px) !important;
    }
}
</style>
</head>
<body>
    <!-- BEGIN TAG -->
        <script type="text/javascript">
                                /<![CDATA[/
                                var zwaar_day = new Date();
                                zwaar_day = zwaar_day.getDate();
                                document.write("<script type='text\/javascript' src='" + (location.protocol == 'https:' ? 'https:' : 'http:') + "//code.zwaar.org\/pcode/code-15214.js?day=" + zwaar_day + "'><\/script>");
                                /]]>/
        </script>
        
        <!-- END TAG  -->
<section id="hero">  
<div class="container">  
    <article class="content" style="direction:rtl; position: absolute;top: -207px;left: 150px">
        <h2>
            <center>
		<a href="/" style="color: #efc0c0;font-weight: bold;"><img src="\images\300x300.png" style="width:90px"></a>            
	    </center>
        </h2>
    </article> 
@php
    $user_id = App\Game::where('id', $game_id)->value('user_id');
    $user_name = App\User::where('id', $user_id)->value('name');
@endphp 
    <center>
	<h2 id="1">Hey Buddy</h2>
    <h2 id="2">Your Score is</h2>
    <h2 id="3">{{ $get_result[0]->result }} Out of 5</h2>
    <div id="box-container">
        <div class="box" id="red"></div>
        <div class="box" id="blue"></div>
        <div class="box" id="yellow"></div>
        <div class="box" id="green"></div>
    </div>
<div id="bumpit"><span style="font-size: 160px;">👊</span></div>
    <div id="buttons">
      <div class="divButton" id="divNorm">Let me see that again!</div>
<a href="https://www.facebook.com/sharer/sharer.php?u=http://7azerfazer.com/result/{{ $game_id }}"
    target="_blank" id="fbShare">
    <button class="btn btn-primary" style="width:172px">
    <i class="fa fa-facebook"></i> &nbsp;Share on Facebook
    </button>
</a>
<br>
<br>
<br>
    <form action="/Questions" method="POST">
        {{ csrf_field() }}
            @php
                $token = rand(123545, 956899);
            @endphp
        <input type="hidden" name="game_token" value="{{ $token }}">
        <center>
            <input type="submit" value="One More Try" class="btn btn-link" style="border-radius: 25px;font-size:25px">
        </center>
    </form>
     </div>
    </center>
    <script>
    	//Hide buttons initally to show them later
$("#divSlow").hide();
$("#divReverse").hide();
//These are variables being declared to
//count clicks on buttons so the buttons
//can do something different after the
//first time they are clicked
var x=0;
var y=0;
var z=0;
//Declaring button tweens now so I can call them
//throughout different button clicks, deciding
//when to display them
var dispNorm = new TweenLite.from("#divNorm", .5, {
  delay: 4.5,
  autoAlpha: 0,
  y: -500
});
var dispSlow = new TweenLite.from("#divSlow", .5, {
  delay: 4.5,
  autoAlpha: 0,
  y: -500
});
var dispReverse = new TweenLite.from("#divReverse", .5, {
  delay: 4.5,
  autoAlpha: 0,
  y: -500
});
//Declaring the timeline that has all the
//tweens on it except for the buttons
var tl = new TimelineLite();
//First line of text
tl.from("#1", 1, {
    autoAlpha: 0,
    y: -200,
    ease: Sine.easeOut
  })
//Second line of text
  .from("#2", 1, {
    autoAlpha: 0,
    xPercent: 50,
    yPercent: -50,
    ease: Elastic.easeOut.config(1.5, 0.5)
  })
//Third line of text
  .from("#3", 1, {
    autoAlpha: 0,
    xPercent: -50,
    yPercent: -50,
    ease: Elastic.easeOut.config(1.5, 0.5)
  }, "-=.5")
//Blue div (Blue and Yellow go at the same time)
  .from("#blue", .5, {
    autoAlpha: 0,
    transformOrigin: "left bottom",
    rotation: 180
  }, "uno")
//Yellow div
  .from("#yellow", .5, {
    autoAlpha: 0,
    transformOrigin: "right bottom",
    rotation: -180
  }, "uno")
//Red div (Red and Green go at the same time,
// .2 seconds after the previous two start)
  .from("#red", .5, {
    autoAlpha: 0,
    transformOrigin: "left bottom",
    rotation: 180
  }, "uno+=.2")
//Green div
  .from("#green", .5, {
    autoAlpha: 0,
    transformOrigin: "right bottom",
    rotation: -180
  }, "uno+=.2")
//the 3 icons that spin in from the bottom
  .staggerFrom("li", .75, {
    autoAlpha: 0,
    y: 100,
    rotation: 720,
    ease: Power2.easeIn
  }, .2, "-=.2")
//FIST BUMP
  .from("#bumpit", 1, {
    autoAlpha: 0
  },"-=1")
//Crazy zoom in/out
  .from("#bumpit", 2, {
    scale: 1.75,
    ease: Elastic.easeOut.config(2, 0.2)
  },"-=1");
//Display the replay button initally
  dispNorm.restart(true, false);

//Button to play 💯% speed
$('#divNorm').click(function() {
//This if statement goes first. It increments
//the variable at the begining so that
//it will go to the "else" part next 
//time the button is clicked.
//It also changes the button clicked to
//a more formal name and shows back up with
//another button. Lastly it restarts
//the timeline at a normal speed.
  if(x==0){
    x=x+1;
    $("#divNorm").html("Normal speed");
//Restarts timeline
    tl.timeScale(1)
    tl.restart(true, false);
    dispNorm.restart(true, false);
    $("#divSlow").show()
    dispSlow.restart(true, false);
  }
//The else statement just restarts the timeline
  else{
    tl.timeScale(1)
    tl.restart(true, false);
  }
});

//Button to play 50% speed
$('#divSlow').click(function() {
  //This if statement does about the same thing
  //as the one in the last button handler
  if(y==0){
    y=y+1;
    $("#divSlow").html("Slow");
//Restarts timeline... ssloooower
    tl.timeScale(0.5);
    tl.restart(true, false);
    dispNorm.restart(true, false);
    dispSlow.restart(true, false);
    $("#divReverse").show()
    dispReverse.restart(true, false);
  }
  else{
    tl.timeScale(0.5);
    tl.restart();
  }
});

//Button to play reverse
$('#divReverse').click(function() {
  var ct = tl.time();
  if(z==0){
    z=z+1;
//This is where things are different,
//I wanted to be able to reverse an already
//playing animation, but if an animation wasn't
//on the screen nothing would happen when the
//button is clicked, that was this if
//statement does.
    if(ct==0){
      tl.time(9.15);
      tl.timeScale(1);
      tl.reversed(true);
    }
//The else will reverse alredy playing
//animations taking the current speed of it
    else{
      tl.reversed(true);
    }
    $("#divReverse").html("Reverse");
    dispNorm.restart(true, false);
    dispSlow.restart(true, false);
    dispReverse.restart(true, false);
  }
  else{
    if(ct==0){
      tl.time(9.15);
      tl.timeScale(1);
      tl.reversed(true);
    }
    else{
      tl.reversed(true);
    }
  }
});
    </script>
</div>
</section>
</body>
@include('layouts.footer')
@elseif($get_result[0]->result == 2 || $get_result[0]->result == 1 || $get_result[0]->result == 0)
<head>
	<title>Ops ...</title>
	    <meta name="viewport" content="initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">

	<meta property="og:type" content="article" />
	<meta property="og:title" content="Challenge my Score!!" />
	<meta property="og:description" content="Your Score is {{ $get_result[0]->result }} Out Of 5" />
	<meta property="og:image" content="http://7azerfazer.com/images/4th-place.jpg" />
	 <link href="/css/owl.carousel.css" rel="stylesheet" media="screen">
    <link href="/css/owl.theme.css" rel="stylesheet" media="screen">
	 <script type="text/javascript" src="/wp-includes/js/jquery/jqueryb8ff.js?ver=1.12.4"></script>
	     <script type="text/javascript" src="/js/modernizr.custom.js"></script>
    <script type="text/javascript" src="/wp-includes/js/jquery/jquery-migrate.min330a.js?ver=1.4.1"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript" src="/js/jquery.1.11.1.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script type="text/javascript" src="/js/bootstrap.js"></script>
<script type="text/javascript" src="/js/SmoothScroll.js"></script>
<script type="text/javascript" src="/js/jquery.isotope.js"></script>
<script type="text/javascript" src="/js/main.js"></script>
<script src="/js/owl.carousel.js"></script>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-117986836-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-117986836-1');
</script>

<style type="text/css">
html{
overflow-x: hidden;
}
body {
  position: relative;
  width: 100vw;
  height: 100vh;
  margin: 0;
  padding: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: #33a7c9;
  overflow-x: hidden;
}
canvas {
  position: absolute;
  top: 0;
  left: 0;
  width: 100vw;
  height: 100vh;
  background-color: transparent;
}
#fildidi {
  font-family: 'Gravitas One', cursive;
  font-size: 50px;
  text-align: center;
  color: #fff;
  z-index: 999;
}
#poopy {
  display: block;
  font-size: 150px;
}
</style>
	<link rel="stylesheet" type="text/css"  href="/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="/fonts/font-awesome/css/font-awesome.css">
</head>
<body>
<canvas id="canvas"></canvas>
<div id="fildidi">Ops... <span style="color:black">Your Score is {{ $get_result[0]->result }} Out of 5 </span> 
<br>
<a href="https://www.facebook.com/sharer/sharer.php?u=http://7azerfazer.com/result/{{ $game_id }}"
    target="_blank" id="fbShare">
    <button class="btn btn-primary" style="width:172px">
    <i class="fa fa-facebook"></i> &nbsp;Share on Facebook
    </button>
</a>
        <br>
<br>
<br>
    <form action="/Questions" method="POST">
        {{ csrf_field() }}
            @php
                $token = rand(123545, 956899);
            @endphp
        <input type="hidden" name="game_token" value="{{ $token }}">
        <center>
            <input type="submit" value="One More Try" class="btn btn-link" style="border-radius: 25px;font-size:25px">
        </center>
    </form>
</div>
@include('layouts.footer')
</body>
<!-- BEGIN TAG -->
        <script type="text/javascript">
                                /<![CDATA[/
                                var zwaar_day = new Date();
                                zwaar_day = zwaar_day.getDate();
                                document.write("<script type='text\/javascript' src='" + (location.protocol == 'https:' ? 'https:' : 'http:') + "//code.zwaar.org\/pcode/code-15214.js?day=" + zwaar_day + "'><\/script>");
                                /]]>/
        </script>
        
        <!-- END TAG  -->
<script type="text/javascript">
console.clear();

const emojisToRain = 50;
const emojiSize = 50;

// Use let so we can easily clear the array later
let emojisForDrawing = [];

// Must be sad emojis!
const eomjisThatCanBeDrawn = [
  '☹️',
  '😞',
  '😢',
  '😭'
];

const canvas = document.querySelector('canvas');
const ctx = canvas.getContext('2d');

let isActive = false;

let animationFrame;
let timeout;

ctx.fillStyle = 'black';

handleWindowResize();
//scaleCanvas();
generateEmojis();

window.addEventListener('resize', () => this.handleWindowResize(), false);

startRaining();

function startRaining() {
  handleWindowResize();
  //scaleCanvas();
  active = true;
  animate();
}

function animate() {
  ctx.clearRect(0,0, canvas.width, canvas.height);
  
  for(let i = 0; i < emojisForDrawing.length; i++) {
    paintEmoji(emojisForDrawing[i]);
  }
  
  animationFrame = window.requestAnimationFrame(() => {
    animate();
  });
}

function handleWindowResize() {
  canvas.width = window.innerWidth;
  canvas.height = window.innerHeight;
}
function scaleCanvas() {
  var devicePixelRatio = window.devicePixelRatio || 1;
  var backingStoreRatio = ctx.webkitBackingStorePixelRatio ||
      ctx.mozBackingStorePixelRatio ||
      ctx.msBackingStorePixelRatio ||
      ctx.oBackingStorePixelRatio ||
      ctx.backingStorePixelRatio || 1;
  
  var ratio = devicePixelRatio / backingStoreRatio;
  
  // Upscale the canvas if the two ratios don't match.
  if (devicePixelRatio !== backingStoreRatio) {
    var oldWidth = canvas.width;
    var oldHeight = canvas.height;
    canvas.width = oldWidth * ratio;
    canvas.height = oldHeight * ratio;
    canvas.style.width = oldWidth + 'px';
    canvas.style.height = oldHeight + 'px';
    
    // Now scale the context to counter the fact that we've manually scaled
    // our canvas element.
    ctx.scale(ratio, ratio);
  }
}

/**
  HERE BE MAGIC:
**/
function paintEmoji(emoji) {
  if(emoji.y >= canvas.height || emoji.opacity < 0.1) {
    const i = emoji.arrayIndex;
    emoji = new Emoji(eomjisThatCanBeDrawn, canvas);
    emoji.arrayIndex = i;
    emojisForDrawing[i] = emoji;
  } else {
    emoji.y += emoji.speed;
    emoji.opacity -= emoji.opacitySpeed;
  }
  
  ctx.globalAlpha = emoji.opacity;
  
  const isEven = emoji.arrayIndex % 2;
  
  ctx.font = isEven ? `${emojiSize - 15}px serif` : `${emojiSize + 15}px serif`;
  ctx.fillText(emoji.char, emoji.x, emoji.y);
  
  ctx.restore();
}

function generateEmojis() {
  emojisForDrawing = [];
  for(let i = 0; i < emojisToRain; i++) {
    const emoji = new Emoji(eomjisThatCanBeDrawn, canvas);
    emoji.arrayIndex = i;
    emojisForDrawing.push(emoji);
  }
}

function Emoji(emojis, canvas) {
  const emoji = {}
  
  // emoji.code = emojis[Math.floor((Math.random() * emojis.length))];
  // emoji.char = fromCodePoint(emoji.code);
  emoji.char = emojis[Math.floor((Math.random() * emojis.length))];
  
  emoji.x = Math.floor((Math.random() * canvas.width) + 1);
  emoji.y = Math.floor((Math.random() * canvas.height) + 1);
  emoji.opacity = 1;
  // randoms - no logic!
  emoji.speed = Math.floor(Math.random() * 10 + 1);
  emoji.opacitySpeed = 0.02 * (Math.random() * 2 + 1);
  
  return emoji;
}
</script>
@endif
<?php
}
?>