@if($score <= 15)
<head>
<title>{{ $results[0]->title }}</title>
  <meta property="og:url"           content="{{ url()->current() }}" />
  <meta property="og:type"          content="article" />
  <meta property="og:title"         content="{{ $results[0]->title }}" />
  <meta property="og:description"   content="{{ $results[0]->desc }}" />
  <meta property="og:image"         content="http://www.7azerfazer.com/public/uploads/{{ $results[0]->img }}" />
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-117986836-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-117986836-1');
</script>
</head>

<!-- BEGIN TAG -->
        <script type="text/javascript">
                                /<![CDATA[/
                                var zwaar_day = new Date();
                                zwaar_day = zwaar_day.getDate();
                                document.write("<script type='text\/javascript' src='" + (location.protocol == 'https:' ? 'https:' : 'http:') + "//code.zwaar.org\/pcode/code-15214.js?day=" + zwaar_day + "'><\/script>");
                                /]]>/
        </script>
        
        <!-- END TAG  -->
@php
$url =  url()->current(); 

header('location: https://www.facebook.com/sharer/sharer.php?u=' . $url. '');
@endphp

@elseif( $score <= 22 )
<head>
<title>{{ $results[1]->title }}</title>
  <meta property="og:url"           content="{{ url()->current() }}" />
  <meta property="og:type"          content="article" />
  <meta property="og:title"         content="{{ $results[1]->title }}" />
  <meta property="og:description"   content="{{ $results[1]->desc }}" />
  <meta property="og:image"         content="http://www.7azerfazer.com/public/uploads/{{ $results[1]->img }}" />
  <script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-117986836-1');
</script>
</head>

<!-- BEGIN TAG -->
        <script type="text/javascript">
                                /<![CDATA[/
                                var zwaar_day = new Date();
                                zwaar_day = zwaar_day.getDate();
                                document.write("<script type='text\/javascript' src='" + (location.protocol == 'https:' ? 'https:' : 'http:') + "//code.zwaar.org\/pcode/code-15214.js?day=" + zwaar_day + "'><\/script>");
                                /]]>/
        </script>
        
        <!-- END TAG  -->
@php
$url =  url()->current(); 

header('location: https://www.facebook.com/sharer/sharer.php?u=' . $url. '');
@endphp
@else
<head>
<title>{{ $results[1]->title }}</title>
  <meta property="og:url"           content="{{ url()->current() }}" />
  <meta property="og:type"          content="article" />
  <meta property="og:title"         content="{{ $results[2]->title }}" />
  <meta property="og:description"   content="{{ $results[2]->desc }}" />
  <meta property="og:image"         content="http://www.7azerfazer.com/public/uploads/{{ $results[2]->img }}" />
  <script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-117986836-1');
</script>
</head>

<!-- BEGIN TAG -->
        <script type="text/javascript">
                                /<![CDATA[/
                                var zwaar_day = new Date();
                                zwaar_day = zwaar_day.getDate();
                                document.write("<script type='text\/javascript' src='" + (location.protocol == 'https:' ? 'https:' : 'http:') + "//code.zwaar.org\/pcode/code-15214.js?day=" + zwaar_day + "'><\/script>");
                                /]]>/
        </script>
        
        <!-- END TAG  -->
@php
$url =  url()->current(); 

header('location: https://www.facebook.com/sharer/sharer.php?u=' . $url. '');
@endphp
@endif