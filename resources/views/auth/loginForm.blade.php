@extends('master')
@section('content')
    <body style="background-color: #15151f">
    <div class="container" style="margin-top: -600px ">
        <div class="row centered-form">
            <div class="col-xs-12 col-sm-8 col-md-4 col-sm-offset-2 col-md-offset-4">
                <div class="panel panel-default"  style="height:520px;">
                    <div class="panel-heading">
                        <h3 class="panel-title">Login to Our Plateform</h3>
                    </div>
                    <div class="panel-body" style="height:220px;padding:15px 15px">
                        <form role="form" method="POST" action="/login">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <input type="text" name="mobile" id="mobile" class="form-control input-sm" placeholder="Mobile Number">
                            </div>
                            <div class="form-group row">
			      <div class="col-sm-10">
			        <input type="password" name="password" class="form-control" id="inputPassword3" placeholder="Password">
			      </div>
			    </div>
                            @include('errors.errors')
                            <input type="submit" value="سجل دخول" class="btn btn-success btn-block">
                        </form>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                              <a href="{{url('/register')}}" class="btn btn-link">Don't have an Account?</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    </body>
@endsection
