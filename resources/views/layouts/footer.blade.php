<!-- FOOTER -->
<script type="text/javascript" src="/wp-content/themes/pixiehuge/assets/js/bootstrap.min8a54.js?ver=1.0.0"></script>
<script type="text/javascript" src="/wp-content/themes/pixiehuge/assets/js/owl.carousel8a54.js?ver=1.0.0"></script>
<script type="text/javascript" src="/wp-content/themes/pixiehuge/assets/js/chosen.jquery.min8a54.js?ver=1.0.0"></script>
<script type="text/javascript" src="/wp-content/themes/pixiehuge/assets/js/main8a54.js?ver=1.0.0"></script>
<script type="text/javascript">
  
    jQuery('section#sponsors .owl-carousel').owlCarousel({
        margin:10,
        loop:true,
        autoWidth:false,
        nav: false,
        responsive : {
            0 : {
                items: 1,
                margin:0,
            },
            360 : {
                items: 2,
                margin:50,
            },
            480 : {
                items: 3,
                margin:50,
            },
            768 : {
                items: 3,
                margin:50,
            },
            992 : {
                items: 4,
                margin:100,
            },
            1200 : {
                items: 5,
                margin:100,
            }
        }
    });

    var owl = jQuery('section#sponsors .owl-carousel').owlCarousel();
    jQuery("section#sponsors .leftArrow").click(function () {
        owl.trigger('prev.owl.carousel');
    });

    jQuery("section#sponsors .rightArrow").click(function () {
        owl.trigger('next.owl.carousel');
    });

</script>
<script type="text/javascript" src="../../../platform.twitter.com/widgets8a54.js?ver=1.0.0"></script>
<script type="text/javascript" src="/wp-includes/js/wp-embed.min20fd.js?ver=4.9.2"></script>
<footer class="homepage" style="bottom:0;position:fixed ; left:-10px" >
    <!-- /TOP-CONTAINER -->

    <div class="bottom">
        <div class="container">

            <article class="left">
                <h5 class="website">حزر فزر</h5>

                <div class="social-icons">
                    <a href="#" target="_blank"><i class="fa fa-facebook"></i></a>
                    <a href="#" target="_blank"><i class="fa fa-twitter"></i></a>
                    <a href="#" target="_blank"><i class="fa fa-instagram"></i></a>
                </div>
            </article>
            <!-- /LEFT -->


             <article class="right" style="direction:ltr; font-family: Montserrat-Regular;">
                <h5>Copyright &copy {{ date("Y") }}.  All rights reserved. | Powered by <i class="fa fa-heart-o" aria-hidden="true"></i> Gtsaw</h5>
            </article>
            <!-- /RIGHT -->

        </div>
    </div>
    <!-- /BOTTOM-CONTAINER -->
</footer>
<!-- /FOOTER -->
<!-- Modal -->
<div class="search-modal modal fade" id="searchModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content search-box">
            <div class="modal-body">
                <form role="search" method="get" id="searchform" class="searchform" action="http://themes.pixiesquad.com/pixiehuge/pink-prime/" >
                    <div class="formSearch">
                        <input type="text" value="" name="s" id="s" />
                        <button type="submit" id="searchsubmit">
                            <i class="fa fa-search"></i>
                        </button>
                    </div>
                </form>            </div>
        </div>
    </div>
</div>


<!-- Modal#Menu -->
<script data-cfasync="false" src="../../cdn-cgi/scripts/d07b1474/cloudflare-static/email-decode.min.js"></script>