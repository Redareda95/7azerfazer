<!DOCTYPE html>
<html lang="en-US" class="no-js">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <meta charset="UTF-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>7azer fazer</title>

<meta property="og:type" content="article" />
<meta property="og:title" content="Welcome To 7azer Fazer" />
<meta property="og:description" content="Quizes Site Developed at GTSAW" />
<meta property="og:image" content="http://7azerfazer.com/images/300x300.png" />
  
  <link rel="shortcut icon" href="/images/300x300.png" type="image/x-icon">


    <link rel="dns-prefetch" href="http://platform.twitter.com/" />
    <link rel="dns-prefetch" href="http://fonts.googleapis.com/" />
    <link rel="dns-prefetch" href="http://s.w.org/" />
    <link rel="alternate" type="application/rss+xml" title="PixieHuge - Pink Prime &raquo; Feed" href="feed/index.html" />
    <link rel="alternate" type="application/rss+xml" title="PixieHuge - Pink Prime &raquo; Comments Feed" href="comments/feed/index.html" />
    <link rel="stylesheet" id="pixiehuge-css"  href="/wp-content/themes/pixiehuge/style8a54.css?ver=1.0.0" type="text/css" media="all" />
    <link rel="stylesheet" id="fontawesome-css"  href="/wp-content/themes/pixiehuge/assets/css/font-awesome.min5152.css?ver=1.0" type="text/css" media="all" />
    <link rel="stylesheet" id="bootstrap-css"  href="/wp-content/themes/pixiehuge/assets/css/bootstrap.min8a54.css?ver=1.0.0" type="text/css" media="all" />
    <link rel="stylesheet" id="flagicon-css"  href="/wp-content/themes/pixiehuge/assets/css/flag-icon.min8a54.css?ver=1.0.0" type="text/css" media="all" />
    <link rel="stylesheet" id="owlcarousel-css"  href="/wp-content/themes/pixiehuge/assets/css/owl.carousel.min8a54.css?ver=1.0.0" type="text/css" media="all" />
    <link rel="stylesheet" id="chosen-css"  href="/wp-content/themes/pixiehuge/assets/css/chosen.min8a54.css?ver=1.0.0" type="text/css" media="all" />
    <link rel="stylesheet" id="pixiehuge_main-css"  href="/wp-content/themes/pixiehuge/assets/css/main4bf4.css?ver=1.0.3" type="text/css" media="all" />
    <style id="pixiehuge_main-inline-css" type="text/css">

        /* BBPress */
        div.bbp-submit-wrapper button,
        #bbpress-forums #bbp-search-form #bbp_search_submit,

            /* WooCommerce */
        body.woocommerce-page .woocommerce .return-to-shop a,
        body.woocommerce-page .woocommerce .form-row.place-order input.button.alt,
        body.woocommerce-page .woocommerce .wc-proceed-to-checkout a,
        .woocommerce span.onsale,
        .woocommerce button.button.alt,
        .woocommerce #review_form #respond .form-submit input,
        .woocommerce div.product a.button a:hover,
        .woocommerce #respond input#submit:hover,
        .woocommerce a.button:hover,
        .woocommerce button.button:hover,
        .woocommerce input.button:hover,
        .woocommerce a.remove:hover,

            /* About page */
        section#aboutInfo,
        section#aboutStaff .container.bg ul.header li:after,

            /* Player page */
        section#player-details .content div.equip ul li a:hover,

            /* Match page */
        section#matchRoster .content .roster > li .overlay,

            /* Sidebar */
        aside article #searchform .formSearch button#searchsubmit,

            /* Team section */
        section#teams .boxes .box,
        section#teams .boxes .box .overlay,

            /* Single page */
        section#single-page article.comments form input#submit,
        section#single-page article.comments form button,
        section#single-page article.header div.top-line a.category,
        .search-page .formSearch button#searchsubmit,

            /* Match page */
        section#matches div.container > div.content li.matchBox > a.cta-btn,
        section#matches.match-page li.matchBox:after,

            /* News page */
        section#news .content .news-box a.category,
        section#single-page article.content p input[type="submit"],

            /* Btn */
        section#streams div.container div.content div.list article.streamBox:not(.large) .details.on-hover > a.cta-btn,
        .btn.btn-blue {
            background-color: #ffa80f !important;
        }

        /* Chosen */
        .chosen-container .chosen-results li.highlighted {
            background: #eb2084 !important
        }

        /* About page */
        section#aboutStory .bg article.footer div.right ul li:hover,

            /* Sidebar */
        aside article #searchform .formSearch input,

            /* Section header */
        .section-header article.bottombar ul li.active a,
        .section-header article.bottombar ul li:hover a,

            /* Match page */
        .btn.btn-transparent,

            /* Single page */
        .search-page .formSearch input,
        section#single-page article.content blockquote,

            /* WooCommerce */
        .woocommerce-info,
        body.woocommerce-page .woocommerce .woocommerce-info,
        .woocommerce form.checkout_coupon,

            /* Footer */
        footer .container.top article.box.aboutUs a {
            border-color: #eb2084 !important;
        }

        /* Player page */
        section#player-profile .player-info .right-panel ul.info-section li:hover {
            border-bottom: 1px solid #eb2084 !important;
        }


        /* BBPress*/
        #bbpress-forums div.bbp-breadcrumb p a,
        #bbpress-forums li a,
        #bbpress-forums p.bbp-topic-meta span a,
        #bbpress-forums li.bbp-body .bbp-topic-title a,

            /* NAV */
        .nav-previous a,
        .nav-next a,
        .mo-modal .mo-menu-box .modal-body ul li a,

            /* Header */
        header > nav > .container > ul > li > a > span.title:hover,
        header > nav > .container > ul > li:hover span.title,
        header div.topbar a:hover,
        #cancel-comment-reply-link,

            /* About Page*/
        section#aboutStaff .container.bg ul.tab span.role,
        section#aboutStaff .container.bg ul.header li.active a,
        section#aboutStaff .container.bg ul.header li a:hover,
        section#aboutStory .bg article.footer div.right ul li span.email,
        section#aboutStory .bg article.head > h4,

            /* Sponsors Page*/
        section#sponsor-page .container ul li .content .head a:hover,
        section#cta-sponsor article.content h4,
        section#sponsor-page .container ul li .content a.cta-link,

            /* Team Page*/
        section#team-profile ul.achievements li span.title,
        section#team-profile article.stats ul.left li span.title,
        section#team-profile .team-info .profile-details div.name span,

            /* Match section */
        section#matches div.container > div.content li.matchBox > div.rightBox > div.match-info > span.league,
        section#matches div.container > div.content li.matchBox > div.rightBox > div.stream > a,
        section#match-details .container.bg article.middle .details h5,
        .btn.btn-transparent,

            /* Player Page*/
        section#player-details .content div.equip ul li .details span.name,
        section#player-details .content .stats ul li div.info span.title,
        section#player-profile .player-info .right-panel ul.info-section li:hover span.title,
        section#player-profile .player-info .right-panel ul.profile-details li.social a:not(.stream):hover,
        section#player-profile .player-info .right-panel ul.profile-details li div.name span,

            /* Maps */
        section#mapsPlayed ul li .details span.won,
        section#matchRoster .content .roster > li .details span,

            /* Sidebar */
        aside article #wp-calendar a,
        aside ul li a,
        aside .tagcloud a,

            /* Single page */
        section#single-page article.header span.date,
        section#single-page article.comments ul li article.author div.details h5,
        section#single-page article.comments p:not(.form-submit) a,
        section#single-page article.comments ul li article.author div.details h5 a,
        section#single-page article.content code,
        section#single-page article.content a,
        section#single-page article.content h2:first-child,

            /* Hero section */
        section#hero article.content h4,

            /* Section header */
        .section-header article.bottombar ul li.active a,
        .section-header article.bottombar ul li:hover a,

            /* Stream section */
        section#streams div.container div.content div.list article.streamBox .details > h6,

            /* Twitter section */
        section#twitter div.tw-bg > article.left > h4,

            /* News section */
        section#news .content .news-box > .details > span,
        .wp-caption-text,

            /* WooCommerce */
        body.woocommerce-page .woocommerce .woocommerce-info a.showcoupon,
        body.woocommerce-page .woocommerce .woocommerce-info a,
        body.woocommerce-page .woocommerce .woocommerce-info:before,
        section#boardmembers ul li.dropdown.active button,
        section#boardmembers ul li.dropdown.active .dropdown-menu li.active a,
        .woocommerce div.product a,
        .woocommerce div.product p.price,
        .woocommerce ul.products li.product .price,
        .woocommerce .woocommerce-product-rating a,
        .woocommerce table.shop_table td.product-name a,
        .woocommerce form .lost_password a,
        .woocommerce-info:before,
        .woocommerce a.remove,
        .woocommerce a.added_to_cart,
        .woocommerce-loop-product__title,

            /* Footer */
        footer .bottom .container > article.left a:hover,
        footer .bottom .container > article.left h5,
        footer .container.top article.box ul li > span.date,
        footer .container.top article.box ul li > a > span.email,
        footer .container.top article.box.aboutUs a {
            color: #eb2084 !important;
        }

        /* Match section */
        section#matches div.container > div.content li.matchBox {
            background: rgba(34, 34, 46, 0.08);
            background: -moz-linear-gradient(left, rgba(34, 34, 46, 0.08) 0%, rgba(235,32,132,.08) 100%);
            background: -webkit-gradient(left top, right top, color-stop(0%, rgba(34, 34, 46, 0.08)), color-stop(100%, rgba(235,32,132,.008)));
            background: -webkit-linear-gradient(left, rgba(34, 34, 46, 0.08) 0%, rgba(235,32,132,.08) 100%);
            background: -o-linear-gradient(left, rgba(34, 34, 46, 0.08) 0%, rgba(235,32,132,.08) 100%);
            background: -ms-linear-gradient(left, rgba(34, 34, 46, 0.08) 0%, rgba(235,32,132,.08) 100%);
            background: linear-gradient(to right, rgba(34, 34, 46, 0.08) 0%, rgba(235,32,132,.08) 100%);
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#22222e', endColorstr='#eb2084', GradientType=1 );
        }
        /* Top bar */
        #icon-discord:hover path,

            /* Match */
        section#matches.match-page .navigation .left:hover svg path,
        section#matches.match-page .navigation .right:hover svg path,

            /* Header */
        section#sponsors span.rightArrow svg:hover path,
        section#sponsors span.leftArrow svg:hover path,

            /* Sponsor page */
        section#sponsor-page .container ul li .content a.cta-link svg path,

            /* Team page */
        section#team-profile article.stats ul.left li svg path,

            /* Footer */
        footer .container.top article.box h4 svg path,

            /* Section header */
        .section-header article.topbar h3 svg path {
            fill: #eb2084 !important;
        }

        /* Header */
        header > nav > .container > ul > li > a > span.title {
            text-shadow: 5px 0px 6px rgba(235,32,132,.35);
        }

        /* About page */
        section#aboutStaff .container.bg ul.header li a {
            text-shadow: 1px 0px 1px rgba(235,32,132,.35);
        }

    </style>
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <link rel="stylesheet" id="pixiehuge_responsive-css"  href="wp-content/themes/pixiehuge/assets/css/responsive4bf4.css?ver=1.0.3" type="text/csstext/css" media="all" />
    <link rel="stylesheet" id="google-fonts-css"  href="http://fonts.googleapis.com/css?family=Baloo+Bhai%7CSource+Sans+Pro%3A400%2C700%7CUbuntu%3A400%2C500%2C700%26amp%3Bsubset%3Dlatin-ext&amp;ver=1.0.0" type="text/css" media="all" />
    <script type="text/javascript" src="/wp-includes/js/jquery/jqueryb8ff.js?ver=1.12.4"></script>
    <script type="text/javascript" src="/wp-includes/js/jquery/jquery-migrate.min330a.js?ver=1.4.1"></script>
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="xmlrpc0db0.php?rsd" />
    <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="/wp-includes/wlwmanifest.xml" />
    <meta name="generator" content="WordPress 4.9.2" />
<style>
body{
overflow-y:hidden
}
        .title{
            padding-top: 20px;
            text-align: center;
            color: #eb2084 ;
            font-size: 36px;
            text-transform: uppercase
        }
        .question{
            padding: 20px;
            font-size: 22px;
            background: #c55754 ;
            border-radius: 20px;
            margin: 10px 0;
            color: #f6f6f6;
        }
        .option{
            width: 470px;
            display: inline-block;
            padding: 10px 0 10px 15px;
            vertical-align: middle;
            margin: 10px 0 10px 10px;
            background: #ddd;
            border-radius: 20px;
            color: #000
        }
        .option:hover{
            background: #549c56 ;
            color: #fff;
        }
        .next-btn{
            float: right;
            border: none;
            outline: none;
            background: rgba(255, 25, 255, 0.5);
            width: 100px;
            height: 35px;
            border-radius: 20px;
            cursor: pointer;
            margin: 10px
        }
        .next-btn:hover{
            background: #eb2084 ;
            color: #f6f6f6
        }
        .result{
            color: white;
            height: 100px;
            text-align: center;
            font-size: 75px;
        }
        .option input:checked .option{
            background: #eb2084 ;
            color: #f6f6f6
        }
        footer{
            min-height: 0;
        }
</style>
        <!-- Basic Page Needs
    ================================================== -->
    <!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=9" /><![endif]-->
    <!-- Favicons
    ================================================== -->
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="img/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/apple-touch-icon-114x114.png">

    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css"  href="/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="/fonts/font-awesome/css/font-awesome.css">

    <!-- Slider
    ================================================== -->
    <link href="css/owl.carousel.css" rel="stylesheet" media="screen">
    <link href="css/owl.theme.css" rel="stylesheet" media="screen">

    <!-- Stylesheet
    ================================================== -->
    <link rel="stylesheet" type="text/css" href="/css/responsive.css">

    <link href="http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,700,300,600,800,400" rel="stylesheet" type="text/css">
<style>
.btno{
    border: 2px solid black;
    background-color: white;
    color: black;
    padding: 14px 28px;
    font-size: 16px;
    cursor: pointer;
    border-radius: 15px;

}

.GTSAW{
    border-color: #ff9800;
    color: orange;
}

.GTSAW:hover {
    background: #ff9800;
    color: white;
}

</style>
    <script type="text/javascript" src="/js/modernizr.custom.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-117986836-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-117986836-1');
</script>
</head>
<body class="home blog" >
    <!-- BEGIN TAG -->
        <script type="text/javascript">
                                /<![CDATA[/
                                var zwaar_day = new Date();
                                zwaar_day = zwaar_day.getDate();
                                document.write("<script type='text\/javascript' src='" + (location.protocol == 'https:' ? 'https:' : 'http:') + "//code.zwaar.org\/pcode/code-15214.js?day=" + zwaar_day + "'><\/script>");
                                /]]>/
        </script>
        
        <!-- END TAG  -->
<div id="fb-root"></div>

<header>
    <div class="topbar">
        <div class="container">
            <a href="https://www.facebook.com/GTSAW.NET/" target="_blank"><i class="fa fa-facebook"></i></a>
            <a href="https://twitter.com/GTSAW1" target="_blank"><i class="fa fa-twitter"></i></a>
            <a href="https://www.linkedin.com/in/gtsaw-software-solution-b59007162/" target="_blank"><i class="fa fa-linkedin"></i></a>
        </div>
    </div>
</header>
<!-- /HEADER -->

<section id="hero" style="height:100vh;min-height:100vh" >  
<div class="container" >  
    <article class="content" style="direction:rtl; position: fixed;top: -213px;left: 70px">
        <h2>
            <center>
                <a href="/" style="color: #efc0c0;font-weight: bold;"><img src="\images\300x300.png" style="width:90px"></a>
            </center>
        </h2>
    </article> 
<canvas style="max-height:150vh">
</canvas>

 </div>   
</section>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript" src="/js/jquery.1.11.1.js"></script>
<script type="text/javascript" src="/js/bootstrap.js"></script>
<script type="text/javascript" src="/js/SmoothScroll.js"></script>
<script type="text/javascript" src="/js/jquery.isotope.js"></script>

<script src="/js/owl.carousel.js"></script>

    <!-- Javascripts
    ================================================== -->
<script type="text/javascript" src="/js/main.js"></script>
<script>
var canvas = document.querySelector('canvas');
canvas.width = innerWidth;
canvas.height = 320;
var context = canvas.getContext('2d');

function Node(x, y, dx, dy, radius, color) {
    this.x = x;
    this.y = y;
    this.dx = dx;
    this.dy = dy;
    this.radius = radius;
    this.color = color;
    this.draw = function() {
        context.beginPath();
        context.fillStyle = this.color;
        context.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false);
        context.fill();
    }
    this.update = function() {
        this.x += this.dx;
        this.y += this.dy;
        if (this.x - this.radius < 0 || this.x + radius > canvas.width) {
            this.dx = -this.dx;
        }
        if (this.y - this.radius < 0 || this.y + radius > canvas.height) {
            this.dy = -this.dy;
        }
        this.draw();
    }
}

var nodes = [];

const nodeCount = 80;

for (var i = 0; i < nodeCount; i++) {
    const maxSpeed = 0.5;
    const maxSize = 4;
    const colors = ['#092140', '#024959', '#F2C777', '#F24738', '#BF2A2A'];
    var radius = (Math.random() + 1) * maxSize;
    var x = Math.floor(Math.random() * (innerWidth - 2 * radius) + radius);
    var y = Math.floor(Math.random() * (innerHeight - 2 * radius) + radius);
    var speed = [Math.random() * maxSpeed, -Math.random() * maxSpeed];
    var dx = Math.floor(speed[Math.floor(Math.random() * 2)] * 10) / 10;
    speed = [(Math.random() + 0.5) * maxSpeed, -(Math.random() + 0.5) * maxSpeed];
    var dy = Math.floor(speed[Math.floor(Math.random() * 2)] * 10) / 10;
    var color = colors[Math.floor(Math.random() * 5)];
    nodes.push(new Node(x, y, dx, dy, radius, color));
}

function distance(a, b) {
    return Math.sqrt(Math.pow(a.x - b.x, 2) + Math.pow(a.y - b.y, 2));
}

function Point(x, y) {
    this.x = x;
    this.y = y
}

const span = 100;

function closestNodes(n, arr) {
    let rest = [];
    for (var i = 0; i < arr.length; i++) {
        if (i != n && distance(nodes[i], nodes[n]) < span) {
            rest.push(new Point(arr[i].x, arr[i].y));
        }
    }
    rest.sort(function(a, b) {
        return distance(a, nodes[n]) - distance(b, nodes[n])
    });
    if (rest.length > 0) {
        return new Point(rest[0].x, rest[0].y);
    }
    return -1;
}

function animate() {
    requestAnimationFrame(animate);
    context.clearRect(0, 0, canvas.width, canvas.height);

    for (var i = 0; i < nodes.length; i++) {
        var point = closestNodes(i, nodes);
        if (point != -1) {
            context.beginPath();
            context.strokeStyle = nodes[i].color;
            context.moveTo(nodes[i].x, nodes[i].y);
            context.lineTo(point.x, point.y);
            context.stroke();
        }
    }

    for (var i = 0; i < nodes.length; i++) {
        nodes[i].update();
    }

}

animate();
</script>
  </body>
</html>