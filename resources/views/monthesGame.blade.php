<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>شخصيتك من ميلادك</title>
<link rel="shortcut icon" href="/images/300x300.png" type="image/x-icon">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
	<link href="https://fonts.googleapis.com/css?family=Rakkas" rel="stylesheet">

<script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-117986836-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-117986836-1');
</script>


<style>
	/*************************** DEFAULTS ********************************/
/*************************** DEFAULTS ********************************/
	html{
		overflow-x: hidden
	}

body{
	  color: #FFFFFF;
  background-image: linear-gradient(135deg, #FF5572, #FF7555);
  overflow-x: hidden;
  font-weight: 400;
  line-height: 1.5;
	font-family: 'Rakkas', cursive;
	text-align: center;
	background-color: #222
	 color: #FFFFFF;
  line-height: 1.4em;
	height: 100vh;
}
	.navbar-inverse{
		border: none
	}
	h1{
		font-size: 60px;
		color:#fff;
		font-weight: bolder;

	}



	.footer-distributed{
		position: fixed;
		bottom: 0;
		
	background-color: #292c2f;
	box-shadow: 0 1px 1px 0 rgba(0, 0, 0, 0.12);
	box-sizing: border-box;
	width: 100%;
	text-align: left;
	font: normal 16px ;
font-family: 'Rakkas', cursive;

	padding: 45px 50px;
	margin-top: 80px;
}

.footer-distributed .footer-left p{
	color:  #8f9296;
	font-size: 14px;
	margin: 0;
}

/* Footer links */

.footer-distributed p.footer-links{
	font-size:18px;
	font-weight: bold;
	color:  #ffffff;
	margin: 0 0 10px;
	padding: 0;
}

.footer-distributed p.footer-links a{
	display:inline-block;
	line-height: 1.8;
	text-decoration: none;
	color:  inherit;
}

.footer-distributed .footer-right{
	float: right;
	margin-top: 6px;
	max-width: 180px;
}

.footer-distributed .footer-right a{
	display: inline-block;
	width: 35px;
	height: 35px;
	background-color:  #33383b;
	border-radius: 2px;

	font-size: 20px;
	color: #ffffff;
	text-align: center;
	line-height: 35px;

	margin-left: 3px;
}


@media (max-width: 600px) {

	.footer-distributed .footer-left,
	.footer-distributed .footer-right{
		text-align: center;
	}
    .new-select {
	font-family: 'Rakkas', cursive;
  color: #fff;
  background-color: #ff8400;
	height: 38px;
	width: 86%;
  -webkit-border-radius: 3px;
  -moz-border-radius: 3px;
  border-radius: 3px;
  font: 20px ;
  display: inline-block;
	margin: 20px
}
	.footer-distributed .footer-right{
		float: none;
		margin: 0 auto 20px;
	}

	.footer-distributed .footer-left p.footer-links{
		line-height: 1.8;
	}
}


.svg-icon {
  width: 1em;
  height: 1em;
  -webkit-transform-origin: 50% 50%;
          transform-origin: 50% 50%;
}
.svg-icon__sprite {
  display: none;
}
.svg-icon--rotated-180 {
  -webkit-transform: rotate(180deg);
          transform: rotate(180deg);
}


.color--white {
  color: #FFFFFF;
}

.fill--white {
  fill: #FFFFFF;
}

.stroke--white {
  stroke: #FFFFFF;
}

.color--black {
  color: #000000;
}

.fill--black {
  fill: #000000;
}

.stroke--black {
  stroke: #000000;
}

.color--red {
  color: #FF5572;
}

.fill--red {
  fill: #FF5572;
}

.stroke--red {
  stroke: #FF5572;
}

.color--coral {
  color: #FF6960;
}

.fill--coral {
  fill: #FF6960;
}

.stroke--coral {
  stroke: #FF6960;
}

.color--orange {
  color: #FF7555;
}

.fill--orange {
  fill: #FF7555;
}

.stroke--orange {
  stroke: #FF7555;
}

.color--gray {
  color: #404040;
}

.fill--gray {
  fill: #404040;
}

.stroke--gray {
  stroke: #404040;
}

/*
 *  Dropdown style
 */

.new-select {
	font-family: 'Rakkas', cursive;
  color: #fff;
  background-color: #ff8400;
	height: 38px;
	width: 100%;
  -webkit-border-radius: 3px;
  -moz-border-radius: 3px;
  border-radius: 3px;
  font: 20px ;
  display: inline-block;
	margin: 20px
}

.new-select span {
  height: 35px;
	font-size: 24px;
  line-height: 35px;
  padding: 0px 50px 0px 15px;
  text-align: center;
  display: block;
  cursor: pointer;
  position: relative;
}

.new-select span:hover { color: rgba(255, 255, 255, 0.75); }

.new-select span:before {
  content: "";
  width: 35px;
  height: 35px;
  background-color: rgba(0, 0, 0, 0.15);
  position: absolute;
  left: 0px;
}
.new-select span:after {
  content: "\25BC";
  color: rgba(0, 0, 0, 0.4);
  width: 35px;
  height: 10px;
  font-size: 11px;
  line-height: 35px;
  text-align: center;
  position: absolute;
  left: 0px;
}

.new-select ul {
	font-family: 'Rakkas', cursive;
  width: 100% !important;
  padding: 0px;
  margin: 0px;
  display: none;
}

.new-select ul li {
	font-family: 'Rakkas', cursive;
  color: white;
  background-color: #E48014;
  height: 30px;
  line-height: 30px;
  border-top: 1px solid rgba(0, 0, 0, 0.15);
  padding: 0px 20px;
  font-size: 20px;
  text-align: center;
  list-style: none;
  cursor: pointer;
}

.new-select ul li:hover {
  color: #fff;
  background-color: rgba(0, 0, 0, 0.25);
  border-color: transparent;
}

.new-select ul li:last-child {
  -webkit-border-radius: 0px 0px 3px 3px;
  -moz-border-radius: 0px 0px 3px 3px;
  border-radius: 0px 0px 3px 3px;
}

.flex-grid-center {
  display: flex;
  justify-content: center;
  margin: 5em 0;
	font-family: 'Rakkas', cursive;

}

.fuller-button {
	font-family: 'Rakkas', cursive;

  color: #ff8400;
  background: white;
  border-radius: 0;
  padding: 1.2em 5em;
  font-size: 1.5em;
  transition: background-color 0.3s, box-shadow 0.3s, color 0.3s;
  margin: 1em;
}
	.fuller-button.red {
	font-family: 'Rakkas', cursive;

  box-shadow: inset 0 0 1em rgba(251, 81, 81, 0.4), 0 0 1em rgba(251, 81, 81, 0.4);
  border: #fb5454 solid 2px;
}
.fuller-button.red:hover {
	font-family: 'Rakkas', cursive;

  background-color: #ff8400;
	color: white;
  box-shadow: inset 0 0 0 rgba(251, 81, 81, 0.4), 0 0 1.5em rgba(251, 81, 81, 0.6);
}
	</style>
</head>

<body>

<!-- BEGIN TAG -->
        <script type="text/javascript">
                                /<![CDATA[/
                                var zwaar_day = new Date();
                                zwaar_day = zwaar_day.getDate();
                                document.write("<script type='text\/javascript' src='" + (location.protocol == 'https:' ? 'https:' : 'http:') + "//code.zwaar.org\/pcode/code-15214.js?day=" + zwaar_day + "'><\/script>");
                                /]]>/
        </script>
        
        <!-- END TAG  -->	
<nav class="navbar ">
  <div class="container-fluid">
    <div class="navbar-header">
     <div class="navwrapper">
				<div class="nav">
				<div class="navbar-header logo" >
      <a class="navbar-brand" href="/"><img src="/images/300x300.png" width="100px"></a>
    </div>
									
				</div>
			</div>
	  </div>
  </div>
</nav>





<div class="clearfix"></div>

<div class="container" style="margin-top: 50px">
<div class="row">
	<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
		<h1> حزر فزر</h1>
	</div> 
	</div>
</div>





<div class="clearfix"></div>
<br>
<br><form method="get" action="/monthesGameResult">
<div class="container" style="margin: 0 auto">
	<div class="row">

		<div class="col-lg-4 col-md-4 col-xs-12 col-sm-12">



  
  <div class="old-select">
    
    <select name="month">
     <option  selected disabled>اختر الشهر</option>
     @foreach($monthes as $month)
      <option value="{{ $month->id }}">{{ $month->month }}</option>
     @endforeach
    </select>
    
  </div>
  

		</div>
		
		<div class="col-lg-4 col-md-4 col-xs-12 col-sm-12 header">


  <div class="old-select">
    <select name="day">
     <option  selected disabled>اختر اليوم</option>
    @foreach($days as $day)    
      <option value="{{ $day->id }}">{{ $day->day }}</option>
    @endforeach
    </select>
  </div>
  

		</div>
	<div class="col-lg-4 col-md-4 col-xs-12 col-sm-12">



  
  <div class="old-select">
    
    <select>
     <option  selected disabled>اختر السنة</option>
     @for($i = 1965;$i<=2010;$i++)
      <option>{{ $i }}</option>
     @endfor
    </select>
    
  </div>
  

		</div>
		
		<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
				<div class="flex-grid-center">
                      <button class="pure-button fuller-button red" type="submit"> اعرف شخصيتك الاجرامية </button>
                </div>
			</div>

	</div>
	
	
<svg class="svg-bg" xmlns="http://www.w3.org/2000/svg">
  <defs>
    <circle id="a" cx="1468" cy="133" r="35"></circle>
    <mask id="h" x="0" y="0" width="70" height="70" fill="#fff">
      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#a"></use>
    </mask>
    <circle id="b" cx="236.5" cy="732.5" r="23.5"></circle>
    <mask id="i" x="0" y="0" width="47" height="47" fill="#fff">
      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#b"></use>
    </mask>
    <circle id="c" cx="1120.5" cy="945.5" r="28.5"></circle>
    <mask id="j" x="0" y="0" width="57" height="57" fill="#fff">
      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#c"></use>
    </mask>
    <path id="d" d="M1193.375 302.875l44.6 77.25h-89.2z"></path>
    <mask id="k" x="0" y="0" width="89.201" height="77.25" fill="#fff">
      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#d"></use>
    </mask>
    <path id="e" d="M1610.8 513.3l34.14 59.128h-68.278z"></path>
    <mask id="l" x="0" y="0" width="68.277" height="59.13" fill="#fff">
      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#e"></use>
    </mask>
    <path id="f" d="M51.045 967.864l38.314 66.362H12.73z"></path>
    <mask id="m" x="0" y="0" width="76.629" height="66.362" fill="#fff">
      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#f"></use>
    </mask>
    <path id="g" d="M46.045 311.097l26.5 45.897H19.546z"></path>
    <mask id="n" x="0" y="0" width="52.997" height="45.897" fill="#fff">
      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#g"></use>
    </mask>
  </defs>
  <g style="mix-blend-mode:overlay;" fill="none" fill-rule="evenodd" opacity=".35" stroke="#FFF">
    <use class="svg-bg_element" mask="url(#h)" stroke-width="6" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#a"></use>
    <use class="svg-bg_element" mask="url(#i)" stroke-width="6" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#b"></use>
    <use class="svg-bg_element opacity-anim" mask="url(#j)" stroke-width="6" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#c"></use>
    <path class="stroke-anim" d="M657.5 848.5l46 46" stroke-width="3" stroke-linecap="square"></path>
    <path d="M1360.5 1206.5l89-89M761.5 42.5l42-42" stroke-width="3" stroke-linecap="square"></path>
    <use class="svg-bg_element" mask="url(#k)" stroke-width="6" transform="rotate(90 1193.375 341.5)" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#d"></use>
    <use class="svg-bg_element" mask="url(#l)" stroke-width="6" transform="rotate(45 1610.8 542.863)" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#e"></use>
    <use class="svg-bg_element rotate-anim" mask="url(#m)" stroke-width="6" transform="rotate(45 51.045 1001.045)" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#f"></use>
    <use class="svg-bg_element" mask="url(#n)" stroke-width="6" transform="rotate(20 46.045 334.045)" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#g"></use>
  </g>
</svg>

</div></form>


	

	
		<footer class="footer-distributed">

			<div class="footer-right">

				<a href="#"><i class="fa fa-facebook"></i></a>
				<a href="#"><i class="fa fa-twitter"></i></a>
			</div>

			<div class="footer-left">

				<p>Gtsaw &copy; 2018</p>
			</div>

		</footer>













<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <script>
	jQuery.when( jQuery('.old-select select').hide('fast', function(){ 
  
  var select = jQuery(this);
  var selected = select.find(":selected").text()
  var options = '';
  
  select.find('option').each( function(){
    options += '<li id="' + jQuery(this).val() + '">' + jQuery(this).text() + '</li>';
  });
  
  select.after('<div class="new-select"><span>' + selected + '</span><ul>' + options + '</ul></div>');

}) ).done( function(){
  
  jQuery('.new-select span').on('click', function(){
    jQuery(this).parent().find('ul').toggle('fast');
  });
  
  jQuery('.new-select li').on('click', function(){
    jQuery(this).closest('.old-select').find('select').val( jQuery(this).attr('id') );
    jQuery(this).closest('.new-select').find('span').text( jQuery(this).text() );
    jQuery(this).parent().slideUp('fast');
  });
  
  jQuery.on('click', function(){
    console.log(! jQuery(this).is('.new-select *'));
    if ( ! jQuery(this).is('.new-select *') ) {
      jQuery('.new-select ul').slideUp('fast');
    }
  });

});
	
	</script>
</body>
</html>
