<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	// echo phpinfo();
    return view('welcome');
});

Route::post('/Questions', 'QuestionsController@displayGameView');

//seeking results
Route::post('/submit/{game_id}', 'QuestionsController@addGameResult');
Route::get('/submit/{game_id}', function () {
    return view('welcome');
});
//for result from external site
Route::get('/result/{game_id}', 'QuestionsController@results');
Route::get('/mon/{month_id}/{day_id}', 'QuestionsController@moShare');


//for Quizat
Route::get('/quizat', function () {
		$categories = App\Category::all();
    return view('quizezPage', compact('categories'));
});
Route::get('/quiz/{id}', 'QuestionsController@quizat');

Route::get('/score/{id}', 'QuestionsController@scoresat');

//month game
Route::get('/monthesGame', 'QuestionsController@monthesGame');
Route::get('/monthesGameResult', 'QuestionsController@monthesGameResult');

//For Auth
Route::get('/login', 'LoginController@loginView');
Route::get('/loginAdmin', 'LoginController@loginAdminView');
Route::get("/register", 'RegisterationController@registerView')->name('login');
Route::post('/registered', 'RegisterationController@register');
Route::get('/show', 'RegisterationController@show');
Route::post('/login', 'LoginController@login');
Route::post('/logined', 'LoginController@loginAdmin');
Route::get('/logout', 'LoginController@logout');

Route::get('/redirect', 'SocialAuthFacebookController@redirect');
Route::get('/callback', 'SocialAuthFacebookController@callback');

//admin area
Route::group(['middleware'=>'roles', 'roles'=>'admin'], function(){
	Route::get('/admin', 'AdminController@showPanel');
	Route::get('/users', 'AdminController@showUsers');
	Route::post('/add-roles', 'AdminController@addRole');
	Route::get('/addUser', 'AdminController@addUser');
	Route::post('/registerFromPanel', 'AdminController@register');
	Route::get('/showQuestions', 'AdminController@showQuestions');
	Route::get('/addQuestion', 'AdminController@addQuestion');
	Route::post('/addQuestions', 'AdminController@storeQuestion');
	Route::get('/edit/{id}', 'AdminController@editQuestion');
	Route::patch('/editQuestion/{id}', 'AdminController@applyEdit');
	Route::get('/delete/{id}', 'AdminController@deleteQuestion');
	Route::get('/7azerResults', 'AdminController@results');
	Route::get('/see_details/{id}', 'AdminController@seeDetails');
	Route::get('/statistics', 'AdminController@stats');
	Route::get('/category', 'CategoryController@index');
	Route::post('/creaCat', 'CategoryController@store');
	Route::get('/editCat/{id}', 'CategoryController@edit');
	Route::patch('/editCat/{id}', 'CategoryController@update');
	Route::get('/deleteCat/{id}', 'CategoryController@destroy');
	Route::get('/allQ/{id}', 'CategoryController@show');
	Route::get('/addques', 'QuestAdminController@create');
	Route::post('/addQuest', 'QuestAdminController@store');
	Route::get('/editQ/{id}', 'QuestAdminController@edit');
	Route::get('/deleteQ/{id}', 'QuestAdminController@destroy');
	Route::patch('/editQ/{id}', 'QuestAdminController@update');
	Route::get('/results', 'CategoryController@indexR');
	Route::get('deleteR/{id}', 'CategoryController@destroyR');
	Route::post('/resSto', 'CategoryController@storeR');
	Route::get('/editR/{id}', 'CategoryController@editR');
	Route::patch('/editRR/{id}', 'CategoryController@updateR');
	Route::get('/month', 'AdminController@monthView');
	Route::get('/day', 'AdminController@dayView');
	Route::get('/monthShow/{id}', 'AdminController@monthShow');
	Route::get('/dayShow/{id}', 'AdminController@dayShow');
	Route::patch('/editMonth/{id}', 'AdminController@updateMonth');
    Route::patch('/editDay/{id}', 'AdminController@updateDay');

});

