<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Res extends Model
{
    protected $fillable = [
        'title', 'img', 'category_id'
    ];
    
    public function category ()
    {
    	return $this->belongsTo(Category::class);
    }
}
