<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Category;
use App\Ans;

class Quest extends Model
{
    protected $fillable = [
        'head', 'category_id'
    ];

    public function category()
    {
    	return $this->belongsTo(Category::class);
    }

    public function answers()
    {
    	return $this->hasMany(Ans::class);
    }
}
