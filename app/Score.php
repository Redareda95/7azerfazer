<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Game;
use App\Question;

class Score extends Model
{
    protected $fillable = [
        'question_id', 'game_id', 'choosen_answer','is_correct'
    ];

    public function game()
    {
    	return $this->belongsTo(Game::class);
    }

    public function questions()
    {
    	return $this->belongsTo(Question::class, 'question_id', 'id');
    }
}
