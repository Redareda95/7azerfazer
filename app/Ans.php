<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Quest;

class Ans extends Model
{
    protected $fillable = [
        'quest_id', 'answer', 'points'
    ];

    public function quest()
    {
    	return $this->belongsTo(Quest::class);
    }
}
