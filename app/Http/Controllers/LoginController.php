<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest')->except(['logout']);;
    }

    public function loginView (Request $request)
    {
    	return view('auth.loginForm');
    }

    public function loginAdminView (Request $request)
    {
        return view('auth.adminLoginForm');
    }

    public function login (Request $request)
    {
    	date_default_timezone_set('Africa/Cairo');

        $last_login = User::where('mobile', request(['mobile']))->value('last_login');

        if ($last_login == date("Y-m-d") ) 
        {
            return back()->withErrors(['message'=>'Come Again Tommorrow']);
        }else{
            if(! auth()->attempt(request(['mobile','password'])))
            {
                return back()->withErrors(['message'=>'Not correct informations']);
            }
            
            $update = User::where('mobile', request(['mobile']))->update(['last_login'=>date("Y-m-d")]);

            session()->flash('message', 'Hello ' . \Auth::user()->name . ' ! you have Login Successfully...');

                return redirect('/admin');
        }
    }

    public function loginAdmin (Request $request)
    {
        if(! auth()->attempt(request(['email','password']))){
            return back()->withErrors(['message'=>'Not correct informations']);
        }

        session()->flash('message', 'Hello ' . \Auth::user()->name . ' ! you have Login Successfully...');

       return redirect('/admin');
    }

    public function logout ()
    {
    	auth()->logout();

        session()->flash('message', 'Success Logout...');

    	return redirect('/');
    }
}
