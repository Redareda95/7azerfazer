<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Answer;
use App\Question;
use App\Game;
use App\Score;
use App\Result;
use App\Quest;
use App\Ans;
use App\Category;
use App\Res;
use DB;

class QuestionsController extends Controller
{
   public function __construct()
    {
        //$this->middleware('auth')->except('results');
    }
    
  public function displayGameView (Request $request)
    {
    
      $questions = Question::orderByRaw('RAND()')->take(5)->get();

      $game_token = request('game_token');
      $game_session = new Game;
      $game_session->user_id = 3;
      $game_session->game_token = $game_token;
      $game_session->save();

      $game_id = Game::where('user_id', 3)
              ->where('game_token', $game_token)
              ->value('id');

      foreach ($questions as $question) 
      {
          $ans= new Score;
          $ans->question_id=$question->id;
          $ans->game_id=$game_id;
          $ans->is_correct=0;
          $ans->save();  
      }

      $first_question = Score::where('game_id', $game_id)
                        ->whereNull('choosen_answer')
                        ->first();

      return view('questions', compact('first_question', 'game_id'));

    }

    public function addGameResult (Request $request, $game_id)
    {	    	
        $question_id = $request['question_id']; 

        $correct_answer = Question::where('id', $question_id)->value('correct_answer');
        
        $add_record = Score::where('question_id', $question_id)
                      ->where('game_id', $game_id)
                      ->first();

        $score_count = Score::where('game_id', $game_id)
                       ->whereNotNull('choosen_answer')
                       ->count();                              
        if ($score_count < 5) 
        {
          if ($add_record->choosen_answer == null) 
          {
            if (isset($request['choosen_answer'])) 
            {
              if ($correct_answer === $request['choosen_answer']) 
                {
                  Score::where('question_id', $question_id)
                          ->where('game_id', $game_id)
                          ->update(['is_correct'=>1]);
                }else{
                  Score::where('question_id', $question_id)
                          ->where('game_id', $game_id)
                          ->update(['is_correct'=>0]);
                }
              
            Score::where('question_id', $question_id)
                        ->where('game_id', $game_id)
                        ->update(['choosen_answer'=>$request['choosen_answer']]);
            }else{
              Score::where('question_id', $question_id)
                        ->where('game_id', $game_id)
                        ->update(['choosen_answer'=>'Skipped']);
            }
            $sc_count = Score::where('game_id', $game_id)
                       ->whereNotNull('choosen_answer')
                       ->count(); 

            if ($sc_count < 5) 
            {            
            $first_question = Score::where('game_id', $game_id)
                          ->whereNull('choosen_answer')
                          ->first();

            return view('questions', compact('first_question', 'game_id'));
            }
              $result = Score::where('is_correct', '1')->where('game_id', $game_id)->count();

              $add_res = new Result;
              $add_res->game_id=$game_id;
              $add_res->result= $result;
              $add_res->save();

              $get_result=Result::where('game_id', $game_id)->get();

              return view('result', compact('get_result','game_id'));
                          
          }else{
            $sco_count = Score::where('game_id', $game_id)
                       ->whereNotNull('choosen_answer')
                       ->count();

                  if ($sco_count < 5) 
                  {            
                  $first_question = Score::where('game_id', $game_id)
                                ->whereNull('choosen_answer')
                                ->first();

                  return view('questions', compact('first_question', 'game_id'));
                  }
                  $result = Score::where('is_correct', '1')->where('game_id', $game_id)->count();
                   $add_res = new Result;
                    $add_res->game_id=$game_id;
                    $add_res->result = $result;
                    $add_res->save();

                    $get_result=Result::where('game_id', $game_id)->get();
                    return view('result', compact('get_result','game_id'));
            }                          
        }
   		//Query for number of correct asnwers
   		$result = Score::where('is_correct', '1')->where('game_id', $game_id)->count();

      $add_res = new Result;
                    $add_res->game_id=$game_id;
                    $add_res->result= $result;
                    $add_res->save();

                    $get_result=Result::where('game_id', $game_id)->get();

    	return view('result', compact('get_result', 'game_id'));
    }

    public function results($game_id)
    {
      $get_result=Result::where('game_id', $game_id)->get();

      return view('result', compact('get_result', 'game_id'));

    }		

    public function quizat($id)
    {
      $quests = Quest::where('category_id', $id)->get();
      
      $results = Res::where('category_id', $id)->get();

      $cat = Category::where('id', $id)->first();
      
      return view('quiz', compact('quests', 'cat', 'results'));
    }
    
    public function scoresat (Request $request, $id)
    {
    	$score = $request->r;

    	$results = Res::where('category_id', $id)->get();
    	
     	return view('scoreShares', compact('score', 'results'));
        
      //  return response()->json(array('msg'=>'reda'));
    }
    
    public function monthesGame()
    {
        $monthes = DB::table('monthes')->get();
        $days = DB::table('days')->get();
        
        return view('monthesGame', compact('monthes', 'days'));
    }
    
     public function monthesGameResult (Request $request)
    {
    	$m = $request->month;
    	$d = $request->day;

    	$month = DB::table('monthes')->find($m);
    	
    	$day = DB::table('days')->find($d);

     	return view('monthesGameResult', compact('month', 'day'));
    }
    
    public function moShare($month_id, $day_id)
    {
        $month = DB::table('monthes')->find($month_id);
    	
    	$day = DB::table('days')->find($day_id);

     	return view('monthesGameResult', compact('month', 'day'));
    }
}