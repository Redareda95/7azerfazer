<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\Question;
use App\Answer;
use App\Score;
use App\Game;
use DB;
use App\Result;
use App\Category;

class AdminController extends Controller
{
    public function showPanel()
    {
    	$users = User::count();
	
	$questions = Question::count();
	
	$results = Game::count();
	
	$quizes = Category::count();
	
    	return view('dashboard.admin_panel', compact("users", "questions", "results", "quizes"));
    }
    
    public function showUsers()	
    {
    	$users=User::get();
        return view('dashboard.showUsers', compact('users'));
    }

    public function addRole(Request $request)
    {
    	 $user=User::where('email', $request['email'])->first();
        $user->roles()->detach();

        if($request['role_user'])
        {
            $user->roles()->attach(Role::where('name', 'user')->first());
        }
        if($request['role_admin'])
        {
            $user->roles()->attach(Role::where('name', 'admin')->first());
        }

        return redirect()->back();
    }

    public function addUser()
    {
    	return view('dashboard.addUser');
    }

    public function register (Request $request)
    {
    	$this->validate(request(),[
            'name'=>'required|min:5|max:25',
            'email'=>'required|email',
            'password'=>'required|confirmed'
        ]);

    	$pass=request(['password']);
        $user= new User;
        $user->name=request('name');
        $user->email=request('email');
        $user->password=bcrypt($pass['password']);
        $user->save();

        //to asign user role for a new user
        $user->roles()->attach(Role::where('name', 'user')->first());

        session()->flash('message', 'User created Successfully...');

    	return redirect('/users');
    }

    public function showQuestions()
    {
    	$questions = Question::latest()->simplePaginate(15);
        $answers=Answer::all();
    
    	return view('dashboard.showQuestions', compact('questions', 'answers'));
    }

    public function addQuestion ()
    {
    	return view('dashboard.addQuestion');
    }

    public function storeQuestion (Request $request) 
    {
    	$this->validate(request(),[
    		'question' => 'required',
            'correct_answer' => 'required',
            'answer1'  => 'required',
            'answer2'  => 'required',
            'answer3'  => 'required',
            'answer4'  => 'required'

     	]);

    	$fromForm = Question::create([
    									'question'=>request('question'),
    									'correct_answer'=>request('correct_answer')    	
                                    ]);

        $question_id = Question::where('question', request('question'))->value('id');


            $ans= new Answer;
            $ans->question_id=$question_id;
            $ans->choises=request('answer1');
            $ans->save(); 

            $ans= new Answer;
            $ans->question_id=$question_id;
            $ans->choises=request('answer2');
            $ans->save();

            $ans= new Answer;
            $ans->question_id=$question_id;
            $ans->choises=request('answer3');
            $ans->save();

            $ans= new Answer;
            $ans->question_id=$question_id;
            $ans->choises=request('answer4');
            $ans->save();            

    	session()->flash('message', 'Question Added Successfully...');

    	return redirect('/addQuestion');

    }

    public function editQuestion ($id)
    {
    	$quest=Question::find($id);
    	return view('dashboard.editQuestion', compact('quest'));
    }

    public function applyEdit (Request $request, $id)
    {
    	$this->validate(request(),[
    		'question' => 'required',
            'correct_answer' => 'required',
            'other_choises'  => 'required'
            ]);

    	$ques = new Question;
    	$ques->where('id', $id)->update(['question'=>request('question'), 
            'correct_answer'=>request('correct_answer')]);

        $q_id = Question::where('question', request('question'))->value('id');

        $others = explode(",", request('other_choises'));
        foreach ($others as $choises) 
        {
            $ans= new Answer;
            $ans->where('question_id', $q_id)->update(['choises'=>$choises]);             
        }

        session()->flash('message', 'Question is Updated');
        return redirect('/showQuestions');
    }

    public function deleteQuestion($id)
    {
        DB::table('questions')->where('id', $id)->delete();
        DB::table('answers')->where('question_id', $id)->delete();
        session()->flash('message', 'Question is Deleted');
        return redirect('/showQuestions');
    }

    public function results()
    {
        if (request('order') == 'asc') 
        {
            $games=Game::latest()->get();
        }elseif (request('order') == 'desc') 
        {
            $games=Game::all();
        }else
        {
            $games=Game::all();
        }
       return view('dashboard.results', compact('games'));
    }

    public function seeDetails($id)
    {
        $score=Score::where('game_id', $id)->get();

        $game=Game::find($id);

        return view('dashboard.resultsDetail', compact('game', 'score'));
    }
    
    public function stats () 
    {
    	$query = Score::select(array('scores.question_id', DB::raw("COUNT('*')")))
                    ->groupBy('scores.question_id')
                    ->get();
    	
    	return view('dashboard.stats', compact('query'));
    }
    
    public function monthView()
    {
        $monthes =  DB::table('monthes')->get();
        
        return view('dashboard.monthes', compact('monthes'));
    }
    
    public function monthShow($id)
    {
        $month =  DB::table('monthes')->find($id);

        return view('dashboard.month', compact('month'));
    }
    
    public function updateMonth(Request $request, $id)
    {
        $this->validate(request(),[
            'month'=>'required',
            'image'=>'image|mimes:jpg,jpeg,png,gif|max:2048',
            'result'=>'required'
        ]);

        if(file_exists($request->file('image')))
        {
           $img_name=time() . '.' . $request->image->getClientOriginalExtension();

           DB::table('monthes')->where('id', $id)->update([ 'month'=>request('month'), 'image'=>$img_name, 'result'=>request('result')]);
                     
           $request->image->move(public_path('uploads'), $img_name);

            session()->flash('message', 'Month Updated');

           return redirect('/month');

        }

       DB::table('monthes')->where('id', $id)->update([ 'month'=>request('month'), 'result'=>request('result')]);

        session()->flash('message', 'Month Updated');

        return redirect('/month');        

    }
    
    public function dayView()
    {
        $days =  DB::table('days')->get();
        
        return view('dashboard.days', compact('days'));
    }
    
     public function dayShow($id)
    {
        $day =  DB::table('days')->find($id);
        
        return view('dashboard.day', compact('day'));
    }
    
    public function updateDay(Request $request, $id)
    {
        $this->validate(request(),[
            'day'=>'required',
            'result'=>'required'
        ]);

        DB::table('days')->where('id', $id)->update([ 'day'=>request('day'), 'result'=>request('result')]);

        session()->flash('message', 'Day Updated');

        return redirect('/day');        

    }
    
}
