<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Quest;
use App\Res;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        return view('dashboard.allCats', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(),[
            'name'=>'required',
            'image'=>'required|image|mimes:jpg,jpeg,png,gif|max:2048',
            'describtion'=>'required'
        ]);

        $img_name=time() . '.' . $request->image->getClientOriginalExtension();

        $category= new Category;
        $category->name=request('name');
        $category->image=$img_name;
        $category->describtion=request('describtion');
        $category->save();

        $request->image->move(public_path('uploads'), $img_name);

        session()->flash('message', 'Category Created');

        return redirect('/category');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $questions = Quest::where('category_id', $id)->get();
        
        $results = Res::where('category_id', $id)->get();

        return view('dashboard.questions', compact('questions', 'results'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);

        return view('dashboard.editCat', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate(request(),[
            'name'=>'required',
            'image'=>'image|mimes:jpg,jpeg,png,gif|max:2048',
            'describtion'=>'required'
        ]);

        if(file_exists($request->file('image')))
        {
           $img_name=time() . '.' . $request->image->getClientOriginalExtension();

           Category::where('id', $id)->update([ 'name'=>request('name'), 'image'=>$img_name, 'describtion'=>request('describtion')]);
                     
           $request->image->move(public_path('uploads'), $img_name);

            session()->flash('message', 'Category Updated');

           return redirect('/category');

        }

        Category::where('id', $id)->update([ 'name'=>request('name'), 'describtion'=>request('describtion')]);

        session()->flash('message', 'Category Updated');

        return redirect('/category');        

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Category::find($id)->delete();

        session()->flash('message', 'Category Deleted Successfully...');

        return redirect('/category');
    }
    
    public function destroyR($id)
    {
    	
        Res::find($id)->delete();
	
        session()->flash('message', 'Result Deleted Successfully...');

        return back();
    }
    
     public function storeR(Request $request)
    {
        $this->validate(request(),[
            'title'=>'required',
            'img'=>'required|image|mimes:jpg,jpeg,png,gif|max:2048',
            'desc'=>'required'
        ]);

        $img_name=time() . '.' . $request->img->getClientOriginalExtension();

        $res= new Res;
        $res->title=request('title');
        $res->img=$img_name;
        $res->desc=request('desc');
        $res->category_id=request('category_id');
        $res->save();

        $request->img->move(public_path('uploads'), $img_name);

        session()->flash('message', 'Result Created');

        return redirect('/allQ/'.request('category_id'));

    }
	public function indexR()
	{
		$categoriess = Category::get();
		return view('dashboard.createRes', compact('categoriess'));
	}
	
	 public function editR($id)
    {
        $categories = Category::all();
        
        $result = Res::find($id);

        return view('dashboard.editRes', compact('categories', 'result'));
    }
    
    
    public function updateR(Request $request, $id)
    {
        $this->validate(request(),[
            'title'=>'required',
            'img'=>'image|mimes:jpg,jpeg,png,gif|max:2048',
            'desc'=>'required'
        ]);

        if(file_exists($request->file('img')))
        {
           $img_name=time() . '.' . $request->img->getClientOriginalExtension();

           Res::where('id', $id)->update([ 'title'=>request('title'), 'img'=>$img_name, 'desc'=>request('desc'), 'category_id'=>request('category_id')]);
                     
           $request->img->move(public_path('uploads'), $img_name);

           session()->flash('message', 'Result Updated');

           return redirect('/allQ/'.request('category_id'));

        }

        Res::where('id', $id)->update([ 'title'=>request('title'), 'desc'=>request('desc'), 'category_id'=>request('category_id')]);

        session()->flash('message', 'Result Updated');

        return redirect('/allQ/'.request('category_id'));        

    }

}
