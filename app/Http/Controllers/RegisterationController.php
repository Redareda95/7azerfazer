<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;

class RegisterationController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest');
    }


    public function registerView ()
    {
        return view('auth.registerationForm');
    }

    public function register (Request $request)
    {
    	$this->validate(request(),[
    		'mobile' => 'required|min:11|numeric|unique:users,mobile',
    		'password'=>'required|confirmed'
    	]);

        $pass=request(['password']);
        $user= new User;
        $user->mobile=request('mobile');
        //$user->email=request('email');
        $user->password=bcrypt($pass['password']);
        $user->last_login=date("Y-m-d");
        $user->save();  

        $user->roles()->attach(Role::where('name', 'user')->first());
        auth()->login($user);

        session()->flash('message', 'Congratulates! you have Registered Successfully...');

    	return redirect('/');
    }

}