<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ans;
use App\Quest;
use App\Category;

class QuestAdminController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $categories = Category::all();

        return view('dashboard.createQuest', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(),[
            'head' => 'required',
            'answer1'  => 'required',
            'answer2'  => 'required',
            'answer3'  => 'required',
            'answer4'  => 'required',
            'point1'    => 'required',
            'point2'    =>'required',
            'point3'    => 'required',
            'point4'    =>'required',
    
        ]);

        $fromForm = Quest::create([
                                        'head'=>request('head'),
                                        'category_id'=>request('category_id')     
                                    ]);

        $question_id = Quest::where('head', request('head'))->value('id');

            $ans= new Ans;
            $ans->quest_id=$question_id;
            $ans->answer=request('answer1');
            $ans->points=request('point1');
            $ans->value='first';
            $ans->save(); 

            $ans= new Ans;
            $ans->quest_id=$question_id;
            $ans->answer=request('answer2');
            $ans->points=request('point2');
            $ans->value='second';
            $ans->save();    
            
            $ans= new Ans;
            $ans->quest_id=$question_id;
            $ans->answer=request('answer3');
            $ans->points=request('point3');
            $ans->value='third';
            $ans->save(); 

            $ans= new Ans;
            $ans->quest_id=$question_id;
            $ans->answer=request('answer4');
            $ans->points=request('point4');
            $ans->value='fourth';
            $ans->save();           

        session()->flash('message', 'Question Added Successfully...');

        return redirect('/addques');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $question = Quest::find($id);
        $categories = Category::all();
        return view('dashboard.editQ', compact('question', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate(request(),[
            'head' => 'required',
            'answer0'  => 'required',
            'answer1'  => 'required',  
            'point0'   => 'required',
            'point1'   => 'required',
            'point2'   => 'required',
            'point3'   => 'required',
                   
        ]);

        $updateQ = Quest::where('id', $id)->update(['head' => request('head'), 'category_id' => request('category_id')]);

        $question_id = Quest::where('head', request('head'))->value('id');

        $id_gadwal = Ans::where('quest_id', $question_id)->get();

        for ($i=0; $i<=3 ; $i++) 
        { 
            Ans::where('id', $id_gadwal[$i]->id)->update(['answer'=> request('answer'.$i), 'points' => request('point'.$i)]);
        }

        $category_id = Quest::where('id', $id)->value('category_id');

        session()->flash('message', 'Question is Updated');

        return redirect('/allQ/'.$category_id);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category_id = Quest::where('id', $id)->value('category_id');


        Quest::find($id)->delete();

        session()->flash('message', 'Question Deleted Successfully...');

        return redirect('/allQ/'.$category_id);
    }
}
