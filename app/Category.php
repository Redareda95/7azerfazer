<?php

namespace App;

use Illuminate\Database\Eloquent\Model; 
use App\Quest;
use App\Res;

class Category extends Model
{
    protected $fillable = [
        'name', 'image', 'describtion'
    ];

    public function quests()
    {
    	return $this->hasMany(Quest::class);
    }
    
    public function results()
    {
    	return $this->hasMany(Res::class);
    }
}
