<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Question;

class Answer extends Model
{
    protected $fillable = [
        'question_id', 'choises'
    ];

    public function question()
    {
    	return $this->belongsTo(Question::class);
    }
}
