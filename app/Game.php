<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Score;
use App\Result;


class Game extends Model
{
    protected $fillable = [
        'user_id', 'game_token'
    ];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function score ()
    {
    	return $this->hasMany(Score::class);
    }
    
    public function result()
    {
    	return $this->hasMany(Result::class);
    }
}

